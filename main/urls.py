from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.decorators import permission_required
from django.views.generic import TemplateView
from django.contrib.flatpages import views
from functools import partial
from pay.views import PayView, LogView, BankFileView, BankRecordsListView, ExportPay, DeletePay
from metric.views import ConfigView, YandexVerificationView, DashboardView, TopView
admin.autodiscover()

pr = partial(permission_required, raise_exception=True)

urlpatterns = patterns(
    '',
    url('^logout/', 'django.contrib.auth.views.logout_then_login', name="logout_then_login"),
    url('^login/', 'django.contrib.auth.views.login', name="login"),
    url(r'^log$', pr('pay.can_view_payinfo')(LogView.as_view()), name='log'),
    url(r'^export/(?P<pk>\d+)/$', pr('pay.can_view_payinfo')(ExportPay.as_view()), name='pay_export'),
    url(r'^delete/(?P<pk>\d+)/$', pr('pay.can_del_payinfo')(DeletePay.as_view()), name='pay_delete'),
    url(r'^pay$', pr('pay.can_add_payinfo')(PayView.as_view()), name='pay'),
    url(r'^import_bank$', pr('pay.can_add_payinfo')(BankFileView.as_view()), name='import_bank'),
    url(r'^select_bank/(?P<pk>\d+)/$', pr('pay.can_add_payinfo')(BankRecordsListView.as_view()), name='select_bank'),
    url(r'^config$', ConfigView.as_view(), name='config'),
    url(r'^process_yandex_code$', YandexVerificationView.as_view(), name='process_yandex_code'),
    url(r'^top$', pr('metric.can_view_top')(TopView.as_view()), name='top'),
    url(r'^dasboard_data/', include('metric.urls')),
    url(r'^claims/', include('claim.urls')),
    url(r'^stat/$', pr('metric.can_view_stat')(DashboardView.as_view()), name='dashboard'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^forum/', TemplateView.as_view(template_name='forum.html'), name='forum'),
    url(r'^forum_iframe/', include('spirit.urls')),
)

urlpatterns += patterns(
    '',
    url(r'^ckeditor/', include('ckeditor.urls')),
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += patterns(
    "",
    url(r'^docs/$', views.flatpage, {'url': '/docs/'}, name='docs'),
    url(r'^', include('django.contrib.flatpages.urls')),
)
