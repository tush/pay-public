from django import forms
from .models import PayInfo, BankFile


class PayInfoForm(forms.ModelForm):

    class Meta:
        model = PayInfo
        exclude = ['email_id']


class BankFileForm(forms.ModelForm):

    class Meta:
        model = BankFile
        exclude = []


class SearchForm(forms.Form):
    q = forms.CharField(255, label='', required=False)

