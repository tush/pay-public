# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pay', '0002_auto_20150605_1204'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='payinfo',
            options={'permissions': (('can_view_payinfo', 'Может просматривать журнал платежей'), ('can_add_payinfo', 'Может вносить оплаты'), ('can_del_payinfo', 'Может удалять оплаты')), 'verbose_name_plural': 'PayInfos', 'default_permissions': (), 'verbose_name': 'PayInfo'},
        ),
    ]
