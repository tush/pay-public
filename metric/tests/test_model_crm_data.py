
from metric.models import CrmData
import datetime
from django.core.exceptions import ValidationError
from unittest.mock import Mock, patch

import pytest


@pytest.fixture
def updater_class():
    return Mock()


@pytest.fixture
def CrmDataClean(updater_class):
    CrmData.updater = None
    CrmData.ftp_folder = 'ftp_folder'
    CrmData.updater_class = updater_class
    return CrmData


@pytest.mark.django_db
def test_create():
    cd = CrmData(
        date=datetime.datetime(2014, 12, 15).date(),
        order_count=1,
        sell_count=2,
        return_count=3,
        order_summ=4.1,
        sell_summ=5.2,
        return_summ=6.3
    )
    cd.save()
    assert cd.id is not None


@pytest.mark.parametrize('key', (
    'date',
    'order_count',
    'sell_count',
    'return_count',
    'order_summ',
    'sell_summ',
    'return_summ',
))
@pytest.mark.django_db
def test_required_args(key):
    args = dict(
        date=datetime.datetime(2014, 12, 15).date(),
        order_count=1,
        sell_count=2,
        return_count=3,
        order_summ=4.1,
        sell_summ=5.2,
        return_summ=6.3
    )
    args.pop(key)
    cd = CrmData(**args)
    pytest.raises(ValidationError, cd.full_clean)


@patch('metric.mixins.FTPClient')
def test_get_updater(ftp_class_mock, CrmDataClean, updater_class):
    updater_mock = updater_class.return_value
    updater = CrmDataClean.get_updater()
    assert updater_mock == updater
    updater_class.assert_called_once_with(ftp_class_mock.return_value, "ftp_folder")


def test_updater_created_once(CrmDataClean, updater_class):
    updater = CrmDataClean.get_updater()
    updater2 = CrmDataClean.get_updater()
    assert updater2 == updater
    updater_class.assert_called_once()


@pytest.mark.django_db
@patch.object(CrmData, 'get_updater')
def test_update_from_crm(get_updater):
    get_updater.return_value.get_data.return_value = {
        'order': {
            'count': 125,
            'summ': 625000.32,
        },
        'sell': {
            'count': 112,
            'summ': 442564.0,
        },
        'return': {
            'count': 126,
            'summ': 2100.00,
        },
    }
    date = datetime.datetime.strptime('2014-12-15', '%Y-%m-%d').date()
    CrmData.objects.filter(date=date).delete()
    data_object = CrmData.update(date)
    assert date == data_object.date
    assert data_object.id is not None
    assert 125 == data_object.order_count
    assert 112 == data_object.sell_count
    assert 126 == data_object.return_count
    assert 625000.32 == data_object.order_summ
    assert 442564.00 == data_object.sell_summ
    assert 2100.00 == data_object.return_summ
    updater = get_updater.return_value
    updater.get_data.assert_called_once_with(date)


# TODO: move to mixin tests
@pytest.mark.django_db
def test_update_all(updater_class, CrmDataClean):
    date1 = datetime.datetime(2014, 12, 15).date()
    date2 = datetime.datetime(2014, 12, 16).date()
    updater_class.return_value.scan_dates.return_value = [date1, date2]
    with patch.object(CrmDataClean, 'update') as update_mock:
        CrmDataClean.update_all()
    assert 2 == update_mock.call_count
    call_args_list = update_mock.call_args_list
    assert date1 == call_args_list[0][0][0]


# TODO: move to mixin tests
@pytest.mark.django_db
def test_not_update_exists(CrmDataClean, updater_class):
    date1 = datetime.datetime(2014, 12, 15).date()
    date2 = datetime.datetime(2014, 12, 16).date()
    updater_class.return_value.scan_dates.return_value = [date1, date2]
    CrmDataClean.objects.create(
        date=date1,
        order_count=1,
        sell_count=2,
        return_count=3,
        order_summ=4.1,
        sell_summ=5.2,
        return_summ=6.3
    )
    with patch.object(CrmDataClean, 'update') as update_mock:
        CrmDataClean.update_all()
    assert 1 == update_mock.call_count
    assert ((date2,),) == update_mock.call_args
