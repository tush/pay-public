from lxml import etree
from decimal import Decimal
import os

from django.test import TestCase
from django.utils import timezone

from pay.xmlbuilder import XmlBuilder


class XmlBuilderTest(TestCase):

    def test_xml_has_declaration(self):
        declaration = b"<?xml version='1.0' encoding='windows-1251'?>"
        xmlbuilder = XmlBuilder()
        self.assertIn(declaration, bytes(xmlbuilder))
        self.assertTrue(bytes(xmlbuilder).startswith(declaration))

    def test_has_pair_pay_tags(self):
        xmlbuilder = XmlBuilder()
        self.assertIn('</Документ_Оплата>'.encode('cp1251'), bytes(xmlbuilder))

    def test_has_xmlns(self):
        xmlbuilder = XmlBuilder()
        self.assertIn(
            'xmlns="urn:upload_schema"'.encode('cp1251'), bytes(xmlbuilder))
        self.assertIn(
            'xmlns:dt="urn:schemas-microsoft-com:datatypes"'.encode('cp1251'),
            bytes(xmlbuilder))

    def test_use_attribs(self):
        builder = XmlBuilder()
        builder.date = timezone.datetime.today()
        builder.id = 1235
        builder.source = 'Монетный двор'
        builder.summ = Decimal('123.00')
        builder.comment = 'Комментарий проверочный'
        date_string = builder.date.strftime('%d.%m.%y')
        doc = etree.fromstring(bytes(builder))
        self.assertEqual(doc.tag.rsplit('}')[-1], "Данные")
        node = doc.find('./{*}ОбъектыМДТипа_Документ/{*}Документ_Оплата')
        self.assertDictEqual({
            'ДатаДок': date_string,
            'НомерДок': '1235',
            'Источник': 'Монетный двор',
            'Сумма': '123.00',
            'Комментарий': 'Комментарий проверочный'
        }, dict(node.attrib))

    def test_xml_equals(self):
        sample = """<?xml version="1.0" encoding="windows-1251"?>
           <Данные xmlns="urn:upload_schema" xmlns:dt="urn:schemas-microsoft-com:datatypes">
           <ОбъектыМДТипа_Документ>
               <Документ_Оплата ДатаДок="29.10.14" НомерДок="1" Источник="Webmoney" Сумма="540.00" Комментарий="Оплата с терминала" ></Документ_Оплата>
           </ОбъектыМДТипа_Документ>
           </Данные>
        """
        builder = XmlBuilder()
        builder.date = timezone.datetime(14, 10, 29)
        builder.id = 1
        builder.source = 'Webmoney'
        builder.summ = Decimal('540.00')
        builder.comment = 'Оплата с терминала'
        result = bytes(builder).decode('cp1251')
        self.assertXMLEqual(sample, result)

    def test_comment_is_none(self):
        builder = XmlBuilder()
        builder.comment = None
        self.assertIn('Комментарий=""'.encode('cp1251'), bytes(builder))

    def test_multilane_comment(self):
        builder = XmlBuilder()
        builder.comment = "привет\nмедвед"
        self.assertIn('Комментарий="привет медвед"', bytes(builder).decode('cp1251'))
