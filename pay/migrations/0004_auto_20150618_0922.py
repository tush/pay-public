# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pay', '0003_auto_20150616_0926'),
    ]

    operations = [
        migrations.AddField(
            model_name='emailinfo',
            name='email_uid',
            field=models.IntegerField(unique=True, null=True),
        ),
        migrations.AlterField(
            model_name='emailinfo',
            name='email_id',
            field=models.IntegerField(),
        ),
    ]
