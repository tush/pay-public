from django import forms


class DateIntervalForm(forms.Form):
    date_from = forms.DateField(label='С', input_formats=['%d.%m.%Y', '%Y%m%d'])
    date_to = forms.DateField(label='По', input_formats=['%d.%m.%Y', '%Y%m%d'])
