"use strict";

var app = angular.module('TopApp', []);

app.controller('TopCtrl', function($scope, data) {
    $scope.title = 'Hello';
    $scope.config = {
        sortKey: 'rest',
        sortReverse: true
    };
    $scope.data = angular.fromJson(data);

    $scope.changeSort = function(key) {
        if (key == $scope.config.sortKey) {
            $scope.config.sortReverse = ! $scope.config.sortReverse;
        } else {
            $scope.config.sortKey = key;
        }
    };
});
