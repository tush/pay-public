# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('claim', '0002_auto_20150605_1551'),
    ]

    operations = [
        migrations.AddField(
            model_name='claim',
            name='email_uid',
            field=models.IntegerField(null=True, verbose_name='UID email', unique=True, default=None),
        ),
        migrations.AddField(
            model_name='claim',
            name='is_spam',
            field=models.BooleanField(verbose_name='Спам', default=False),
        ),
    ]
