
from lxml import etree
from io import BytesIO
import datetime
import os


class FtpImporter(object):

    def __init__(self, ftp_client, base_dir):
        self.client = ftp_client
        self.base_dir = base_dir

    def _get_report_path(self, date):
        filename = date.strftime('%d-%m-%Y.xml')
        return os.path.join(self.base_dir, filename)

    def _get_xml(self, date):
        path = self._get_report_path(date)
        return self.client.get(path).read()

    def _parse_xml(self, xml):
        raise NotImplementedError()

    def get_data(self, date):
        xml = self._get_xml(date)
        return self._parse_xml(xml)

    def scan_dates(self):
        files = self.client.dir(self.base_dir)
        dates = []
        for filename in files:
            try:
                dt = datetime.datetime.strptime(filename, '%d-%m-%Y.xml')
                date = dt.date()
                dates.append(date)
            except ValueError:
                pass
        return dates


class CrmImporter(FtpImporter):

    def _parse_xml(self, xml):
        buf = BytesIO(xml)
        doc = etree.parse(buf)
        data = {}
        for node in doc.iter(tag='data'):
            count = int(node.attrib['count'])
            summ = float(node.attrib['sum'])
            data[node.attrib['Type']] = {
                'count': count,
                'summ': summ,
            }
        data['return'] = data['returnCnt']
        del data['returnCnt']
        return data


class TopImporter(FtpImporter):

    def _parse_xml(self, xml):
        buf = BytesIO(xml)
        doc = etree.parse(buf)
        items = []
        for node in doc.iter(tag='Nomenclature'):
            item = {}
            for attr in node:
                val = attr.text
                if val.isdigit():
                    val = int(val)
                item[attr.tag] = val
            items.append(item)
        return items
