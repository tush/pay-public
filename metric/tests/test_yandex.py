from django.test import TestCase
from django.test.utils import override_settings
from metric.yandex import Metrika, GetTokenException, NoTokenException
from unittest.mock import patch
import datetime


class MetrikaTest(TestCase):

    def test_init(self):
        m = Metrika(token="123")
        assert "123" == m.token

    @override_settings(METRIKA_APP_ID="444")
    def test_confirm_code_url_generate(self):
        m = Metrika()
        assert "https://oauth.yandex.ru/authorize?response_type=code&client_id=444" == m.confirm_code_url

    @override_settings(METRIKA_APP_ID="444",
                       METRIKA_APP_SECRET="567")
    def test_get_token(self):
        m = Metrika()
        with patch('metric.yandex.requests', **{
            'post.return_value.json.return_value': {
                "access_token": "555",
                "expires_in": 789,
            }
        }) as r_mock:
            token = m.get_token(code="123")
        assert "555" == token
        assert "555" == m.token
        r_mock.post.assert_called_once_with('http://oauth.yandex.ru/token', {
            'grant_type': 'authorization_code',
            'code': '123',
            'client_id': '444',
            'client_secret': '567',
        })

    def test_error_get_token(self):
        m = Metrika()
        with patch('metric.yandex.requests', **{
            'post.return_value.json.return_value': {
                "error_description": "описание ошибки",
                "error": "error_code",
            }
        }):
            with self.assertRaises(GetTokenException) as e:
                m.get_token(code="123")
        assert "error_code: описание ошибки" == str(e.exception)

    def test_make_query(self):
        m = Metrika(token="333")
        data = {
            "result": "555",
            "expires_in": 789,
        }
        with patch('metric.yandex.requests', **{
            'get.return_value.json.return_value': data
        }) as r_mock:
            result = m._make_query(path="/test", args={'date1': '20141112'})
        r_mock.get.assert_called_once_with('http://api-metrika.yandex.ru/test.json', params={
            'date1': '20141112',
            'oauth_token': "333"
        })
        assert data == result

    def test_make_query_wo_token(self):
        m = Metrika()
        data = {
            "result": "555",
            "expires_in": 789,
        }
        with patch('metric.yandex.requests', **{
            'get.return_value.json.return_value': data
        }) as r_mock:
            result = m._make_query(path="/test", args={'date1': '20141112'})
        r_mock.get.assert_called_once_with('http://api-metrika.yandex.ru/test.json', params={
            'date1': '20141112'
        })
        assert data == result

    def test_get_counters(self):
        m = Metrika(token="222")
        with patch.object(m, '_make_query', return_value=[{'id': 1}, {'id': 2}]) as make_query_mock:
            counters = m.get_counters()
        make_query_mock.assert_called_once_with("/counters")
        assert 2 == len(counters)
        assert 2 == counters[1].get('id')

    def test_get_counters_wo_token(self):
        m = Metrika()
        with self.assertRaises(NoTokenException):
            m.get_counters()

    def test_get_day_data(self):
        day = "20141218"
        day_obj = datetime.datetime.strptime(day, "%Y%m%d").date()
        m = Metrika('token')
        day_data = {
            'page_views': 234,
            'denial': 0.12,
            'visit_time': 22,
        }
        with patch.object(m, '_make_query', return_value={
            'data': [
                day_data,
            ],
        }) as make_query_mock:
            result = m.get_day_data(987, day_obj)
        make_query_mock.assert_called_once_with("/stat/traffic/summary", {
            'id': 987,
            'date1': day,
            'date2': day,
        })
        assert day_data == result

    def test_get_day_data_wo_token(self):
        m = Metrika()
        with self.assertRaises(NoTokenException):
            m.get_day_data(123, datetime.date.today())
