from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import FormView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.list import ListView
from django.views.generic.base import RedirectView
from django.contrib import messages

from .models import PayInfo, BankFile, BankRecord
from .forms import PayInfoForm, BankFileForm, SearchForm


class LogView(ListView):
    model = PayInfo
    template_name = 'pay/log.html'
    paginate_by = 20

    def get_form(self):
        return SearchForm(self.request.GET)

    def get_queryset(self):
        qs = super(LogView, self).get_queryset().order_by("-date", "-id")
        form = self.get_form()
        if form.is_valid():
            qs = qs.search(form.cleaned_data['q'])
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args,  **kwargs)
        context['form'] = self.get_form()
        return context


class PayView(FormView):
    template_name = 'pay/pay_form.html'
    form_class = PayInfoForm
    second_form_class = BankFileForm
    success_url = reverse_lazy('log')

    def form_valid(self, form):
        form.save()
        return super(PayView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PayView, self).get_context_data(**kwargs)
        if 'form2' not in context:
            context['form2'] = self.second_form_class()
        return context


class BankFileView(FormView):
    template_name = 'pay/import_file.html'
    form_class = BankFileForm
    # success_url = reverse_lazy('select_bank', )
    #

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('select_bank', kwargs={'pk': self.pk})

    def form_valid(self, form):
        obj = form.save()
        self.pk = obj.pk
        return super(BankFileView, self).form_valid(form)


class BankRecordsListView(SingleObjectMixin, ListView):
    template_name = 'pay/bank_records_form.html'
    context_object_name = 'file'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=BankFile.objects.all())
        return super(BankRecordsListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BankRecordsListView, self).get_context_data(**kwargs)
        context['bank_file'] = self.object
        return context

    def get_queryset(self):
        self.queryset = self.object.records.all()
        return super(BankRecordsListView, self).get_queryset()

    def post(self, *args, **kwargs):
        ids = self.request.POST.getlist('id')
        bank_file = self.get_object(queryset=BankFile.objects.all())
        records = bank_file.records.filter(id__in=ids)
        if records:
            for record in records:
                PayInfo.objects.create(
                    summ=record.summ,
                    date=record.date,
                    comment='{r.payer}\n{r.comment}'.format(r=record),
                    source=PayInfo.SOURCE_BANK,
                )
            bank_file.delete()
            return redirect('log')
        return self.get(*args, **kwargs)


class ExportPay(SingleObjectMixin, RedirectView):
    model = PayInfo
    url = reverse_lazy('log')
    permanent = False

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.export()
        messages.success(request, 'Оплата "{0.id}" поставлена в очередь на перевыгрузку'.format(self.object))
        return super().get(request, *args, **kwargs)


class DeletePay(SingleObjectMixin, RedirectView):
    model = PayInfo
    url = reverse_lazy('log')
    permanent = False

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        messages.success(request, 'Оплата "{0.id}" удалена'.format(self.object))
        self.object.delete()
        return super().get(request, *args, **kwargs)
