from xml.etree.ElementTree import Element, tostring
from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
from lxml import html as lh
from lxml.html import defs
import re
from pay.imap_fetcher import ImapFetcher
from pay.models import FtpExporter
from .tasks import export_claim


class ClaimFtpExporter(FtpExporter):
    base_folder = settings.FTP_FOLDER_CLAIM


class XmlDict(dict):

    def to_xml(self, root_tag='root'):
        elem = Element(root_tag)
        for key, val in self.items():
            child = Element(key)
            child.text = str(val)
            elem.append(child)
        return tostring(elem, encoding='windows-1251')
        # return b'<?xml version="1.0" encoding="windows-1251"?>\n' + xml_bytes


class Claim(models.Model):

    DELIVERY_POST = "post"
    DELIVERY_COURIER = "courier"
    DELIVERY_SELF = "self"
    DELIVERY_EMS = "ems"
    DELIVERY_TRANSPORT = "transport"
    DELIVERY_COURIER_RUSSIA = "courier_russia"
    DELIVERY_SELF_PROLETARSKAYA = "self_proletarskaya"
    DELIVERY_SELF_STROGINO = "self_strogino"
    DELIVERY_SELF_SCHUKINSKAYA = "self_schukinskaya"

    DELIVERY_CHOISES = (
        (DELIVERY_POST, 'Почтой'),
        (DELIVERY_COURIER, 'Курьером'),
        (DELIVERY_SELF, 'Самовывоз'),
        (DELIVERY_EMS, 'Экспресс доставка через EMS почту'),
        (DELIVERY_TRANSPORT, 'Транспортная компания'),
        (DELIVERY_COURIER_RUSSIA, 'Курьерская доставка в города России'),
        (DELIVERY_SELF_PROLETARSKAYA, 'Самовывоз Пролетарская'),
        (DELIVERY_SELF_STROGINO, 'Самовывоз Строгино'),
        (DELIVERY_SELF_SCHUKINSKAYA, 'Самовывоз Щукинская'),
    )

    PAY_CASH = 'cash'
    PAY_BANK = 'bank'
    PAY_EMONEY = "emoney"
    PAY_OTHER = "other"

    PAY_CHOICES = (
        (PAY_CASH, 'Наличными при получении'),
        (PAY_BANK, 'Оплата через банк'),
        (PAY_EMONEY, 'Электронные деньги'),
        (PAY_OTHER, 'Другой способ оплаты'),
    )

    sender = models.CharField('Отправитель', max_length=255)
    subject = models.CharField('Тема', max_length=255, null=True, blank=True)
    message = models.TextField('Сообщение', null=True, blank=True)
    comment = models.TextField('Комментарий оператора', blank=True, null=True)
    order_num = models.IntegerField('Номер заказа', blank=True, null=True)
    delivery_method = models.CharField(
        'Способ доставки', max_length=20, choices=DELIVERY_CHOISES, blank=True, null=True)
    pay_form = models.CharField('Способ оплаты', max_length=50, choices=PAY_CHOICES, blank=True, null=True)
    manager = models.ForeignKey(User, verbose_name='Менеджер', null=True)
    sended_on = models.DateTimeField('Дата отправки', default=timezone.now, db_index=True)
    is_spam = models.BooleanField(u'Спам', default=False, db_index=True)
    email_uid = models.IntegerField(u'UID email', null=True, default=None, unique=True)
    seen = models.BooleanField(u'Просмотрено', default=False)
    exported = models.BooleanField(u'Выгружено', default=False)

    class Meta:
        verbose_name = "Обращение"
        verbose_name_plural = "Обращения"
        default_permissions = ()
        permissions = (
            ('can_work_with_claims', "Может работать с обращениями"),
        )
        ordering = ['-sended_on']

    def __str__(self):
        return "Обращение {0.pk}: {0.subject}".format(self)

    def get_absolute_url(self):
        return reverse('edit-claim', kwargs={'pk': self.pk})

    def export(self):
        data = XmlDict(
            sender=self.sender,
            subject=self.subject,
            message=self.message,
            comment=self.comment,
            order_num=self.order_num,
            delivery_method=self.get_delivery_method_display(),
            pay_form=self.get_pay_form_display(),
            manager=self.manager,
            sended_on=self.sended_on,
        )
        filename = "{}.xml".format(self.id)
        export_claim.apply_async((data.to_xml(), filename))

    @property
    def is_seen_display(self):
        value = self.seen
        if not self.seen:
            self.seen = True
            self.save(update_fields=['seen'])
        return value

    @classmethod
    def update_from_email(cls):
        latest_email = cls.objects.exclude(email_uid=None).order_by('-email_uid').first()
        if latest_email is None:
            last_uid = 0
        else:
            last_uid = latest_email.email_uid
        fetcher = ImapFetcher(
            settings.IMAP_HOST,
            settings.IMAP_PORT,
            settings.IMAP_CLAIM_LOGIN,
            settings.IMAP_CLAIM_PASSWORD)
        ids = fetcher.get_ids_greater(last_uid)
        for m_id in ids:
            data = fetcher.get_message_info(m_id)
            if cls.objects.filter(email_uid=data.uid).exists():
                continue
            obj = cls(
                email_uid=data.uid,
                subject=data.subject,
                sender=data.sender
            )
            # obj.sended_on = fetcher.get_message_date(m_id)
            content, date, content_subtype = fetcher.get_email_content(m_id, desired_part=('text', 'plain'))
            obj.sended_on = date
            if len(content) > 10 and content_subtype.lower() == 'html':
                doc = lh.fromstring(content)
                for node in doc.iter():
                    if node.tag in defs.block_tags:
                        if node.text is not None:
                            node.text += "\n"
                    elif node.tag == 'br':
                        if node.tail is not None:
                            node.tail += '\n'
                        else:
                            node.tail = '\n'
                content = lh.tostring(doc, method='text', encoding=str, pretty_print=True)
            content = re.sub('(\r?\n)+', '\n', content)
            obj.message = content.strip()
            obj.save()
