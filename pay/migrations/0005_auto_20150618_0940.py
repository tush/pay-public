# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pay', '0004_auto_20150618_0922'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emailinfo',
            name='email_id',
            field=models.IntegerField(null=True),
        ),
    ]
