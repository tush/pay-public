# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def set_profiles(apps, schema_editor):
    Profile = apps.get_model("spirit", "UserProfile")
    User = apps.get_model("auth", "User")
    for user in User.objects.all():
        if not Profile.objects.filter(user=user).exists():
            Profile.objects.create(user=user)


class Migration(migrations.Migration):

    dependencies = [
        ('pay', '0006_remove_emailinfo_email_id'),
        ('spirit', '0010_auto_20150505_2125'),
    ]

    operations = [
        migrations.RunPython(set_profiles),
    ]
