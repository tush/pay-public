import imaplib
import email.utils
from django.utils import timezone
import re
import logging
from collections import namedtuple
import chardet
import base64
import codecs
from pyparsing import (nestedExpr, Literal, Word, alphanums,
                       quotedString, replaceWith, nums, removeQuotes, Suppress)

imaplib._MAXLINE = 40000
logger = logging.getLogger(__name__)

EmailData = namedtuple('EmailData', 'subject, from_addr, sender, uid, body')


def get_decoded(msg):
    encoding = next(filter(None, msg.get_charsets() + ['utf-8']))
    try:
        content = msg.get_payload(decode=True)
    except Exception as e:
        logger.warning(e)
        content = msg.get_payload()
    if isinstance(content, bytes) and encoding:
        try:
            content = content.decode(encoding)
        except UnicodeDecodeError:
            content = msg.get_payload()
    try:
        content = content.encode('latin1')
        content = base64.decodestring(content)
    except Exception as e:
        logger.warning(e)
        pass
    return content


class ImapFetcher(object):

    def __init__(self, host, port, login, password):
        if port == 993:
            self._m = imaplib.IMAP4_SSL(host, port)
        else:
            self._m = imaplib.IMAP4(host, port)
        self._m.login(login, password)
        self._m.select()

    def get_ids(self, condition):
        yes, nums = self._m.search(None, condition)
        if yes != 'OK':
            logger.warning('Нет писем для строки поиска: {}'.format(condition))
            return []
        nums = nums[0].split()
        return [int(x) for x in nums]

    def get_ids_from_date_for_senders(self, date, senders=[]):
        date_string = date.strftime("%d-%b-%Y")
        base_condition = 'NOT BEFORE {0}'.format(date_string)
        ids = []
        if len(senders):
            for sender in senders:
                condition = '({} FROM {})'.format(base_condition, sender)
                ids += self.get_ids(condition)
        else:
            condition = '({})'.format(base_condition)
            ids = self.get_ids(condition)
        return tuple(set(ids))

    def get_ids_greater(self, last_uid=0):
        condition = "UID {}:*".format(last_uid + 1)
        ids = self.get_ids(condition)
        return tuple(set(ids))

    def _parse_imap_answer(self, answer):

        accum = b''
        for block in answer:
            skip_next = False
            if isinstance(block, bytes):
                block = (block,)
            for line in block:
                if skip_next:
                    skip_next = False
                    continue
                ending = re.search(b'{\d+}$', line)
                if ending is not None:
                    accum += line[:ending.span()[0]]
                    skip_next = True
                else:
                    accum += line
        NIL = Literal("NIL").setParseAction(replaceWith(None))
        integer = Word(nums).setParseAction(lambda t: int(t[0]))
        quotedString.setParseAction(removeQuotes)
        content = (NIL | integer | Word(alphanums))

        num, data = accum.split(b' ', 1)
        data = data.decode('latin1')
        return nestedExpr(content=content, ignoreExpr=quotedString).parseString(data).asList()

    def get_email_info(self, message_id):
        result = {}
        yes, answer = self._m.fetch(str(message_id), '(BODYSTRUCTURE INTERNALDATE)')
        if yes != "OK":
            logger.warning('Ошибка загрузки сведений о письме {}'.format('message_id'))
            return None
        logger.info("Answer: %s" % answer)
        data = self._parse_imap_answer(answer)
        data = dict(zip(data[0][::2], data[0][1::2]))
        body_data = data['BODYSTRUCTURE']
        message_date = email.utils.parsedate_to_datetime(data['INTERNALDATE'])
        multipart = True
        boundary_container = body_data
        part_n = 0
        if isinstance(body_data[0], str):
            # single
            part = body_data
            multipart = False
        elif isinstance(body_data[0][0], list):
            # multipart with attach
            part = body_data[0][0]
            boundary_container = body_data[0]
            if isinstance(body_data[0][0][0], list):
                part = part[0]
                boundary_container = boundary_container[0]
        else:
            # multipart
            part = body_data[0]
        boundary = None
        for i, chunk in enumerate(boundary_container):
            if isinstance(chunk, list) and isinstance(chunk[0], str) and chunk[0].lower() == 'text':
                part_n = i
                if chunk[1].lower() == 'plain':
                    break
        for chunk in boundary_container:
            if isinstance(chunk, list) and isinstance(chunk[0], str) and chunk[0].lower() == 'boundary':
                boundary = chunk[1]
                break
        logger.info("Body data: %s" % body_data)
        content_type, content_subtype,  charset, _, _, transfet_encoding, *_ = part
        if charset is not None:
            for name, val in zip(charset[::2], charset[1::2]):
                if name.lower() == "charset":
                    charset = val
                    break
            else:
                charset = None
        return content_type, content_subtype, charset, transfet_encoding, message_date, multipart, boundary, part_n

    def get_email_content(self, message_id, desired_part=None):
        '''
        desired_part - tuple('text', 'html')
        '''
        encoding = 'utf-8'
        if desired_part is not None:
            part = '(BODY.PEEK[TEXT])'
        else:
            part = '(BODY.PEEK[])'
        yes, body = self._m.fetch(str(message_id), part)
        if yes != 'OK':
            logger.warning('Ошибка загрузки письма {}'.format(message_id))
            return None
        content = body[0][1]
        if desired_part is not None:
            (
                content_type,
                content_subtype,
                charset,
                transfet_encoding,
                message_date,
                multipart,
                boundary,
                part_n
            ) = self.get_email_info(message_id)
            if multipart:
                content = content.split(boundary.encode('latin1'))[part_n + 1].split(b'\r\n\r\n')[1]
            if transfet_encoding not in ('7bit', '8bit', None):
                try:
                    content = codecs.decode(content, transfet_encoding)
                except Exception as e:
                    logger.warning(e)
                    raise
            for enc in filter(None, (charset, chardet.detect(content)['encoding'], 'utf-8', 'cp1251', 'koi8-r')):
                try:
                    content = content.decode(enc)
                    break
                except UnicodeDecodeError:
                    pass
            else:
                content = content.decode('utf-8', errors="replace")
            return content, message_date, content_subtype
        else:
            return content

    def get_uid_for_id(self, message_id):
        yes, resp = self._m.fetch(str(message_id), '(UID)')
        if yes != 'OK':
            logger.warning('Ошибка получения UID по message_id: {}'.format(message_id))
            return None
        match = re.search(b'\(UID (?P<uid>\d+)', resp[0])
        if match is None:
            return None
        return int(match.group('uid'))

    def get_message_info(self, message_id):
        yes, result = self._m.fetch(
            str(message_id), '(BODY[HEADER] UID)')
        if yes != 'OK':
            raise Exception(result)
        header = result[0][1]
        Ints = Word(nums).setParseAction(lambda t: int(t[0]))
        Uid = (Literal('UID').suppress() + Ints)
        uid = Uid.searchString(result[1])[0][0]
        if isinstance(header, str):
            msg = email.message_from_string(header)
        elif isinstance(header, bytes):
            msg = email.message_from_bytes(header)
        else:
            raise Exception('email_string is not string (or bytes)')
        subject = msg.get('subject')
        if subject is not None:
            subject = email.header.decode_header(subject)[-1][0]
        encoding = next(filter(None, msg.get_charsets() + ['utf-8']))
        if isinstance(subject, bytes):
            try:
                subject = subject.decode(encoding)
            except UnicodeDecodeError:
                encoding = chardet.detect(subject)['encoding']
                subject = subject.decode(encoding, errors="replace")
        from_addr = email.header.decode_header(
            msg.get('from'))[-1][0]
        if isinstance(from_addr, bytes):
            from_addr = from_addr.decode('latin1')
        _, sender = email.utils.parseaddr(from_addr)
        return EmailData(uid=uid, subject=subject, sender=sender, from_addr=from_addr, body=None)

    def get_message_header(self, message_id, header):
        yes, result = self._m.fetch(
            str(message_id), '(BODY[HEADER.FIELDS ({})])'.format(header.upper()))
        if yes != 'OK':
            raise Exception(result)
        return result[0][1].decode('latin-1').split(header + ':')[-1].strip()

    def get_message_date(self, message_id):
        try:
            date_string = self.get_message_header(message_id, 'Date')
        except:
            return timezone.now()
        if date_string == '':
            return timezone.now()
        return email.utils.parsedate_to_datetime(date_string)

    def get_message_sender(self, message_id):
        return self.get_message_header(message_id, 'From')

    def get_message_subject(self, message_id):
        return self.get_message_header(message_id, 'Subject')


def parse_email(email_string):
    if isinstance(email_string, str):
        msg = email.message_from_string(email_string)
    elif isinstance(email_string, bytes):
        msg = email.message_from_bytes(email_string)
    else:
        import pdb; pdb.set_trace()
        raise Exception('email_string is not string (or bytes)')
    subject = msg.get('subject')
    if subject is not None:
        subject = email.header.decode_header(subject)[-1][0]
    encoding = next(filter(None, msg.get_charsets() + ['utf-8']))
    if isinstance(subject, bytes):
        try:
            subject = subject.decode(encoding)
        except UnicodeDecodeError:
            encoding = chardet.detect(subject)['encoding']
            subject = subject.decode(encoding, errors="replace")
    from_addr = email.header.decode_header(
        msg.get('from'))[-1][0]
    if isinstance(from_addr, bytes):
        from_addr = from_addr.decode('latin1')
    _, sender = email.utils.parseaddr(from_addr)
    msg_parts = {}
    for part in msg.walk():
        if part.get_content_maintype() == 'text':
            msg_parts[part.get_content_type()] = get_decoded(part)
    if 'text/plain' in msg_parts:
        body = msg_parts['text/plain']
    elif 'text/html' in msg_parts:
        body = msg_parts['text/html']
    else:
        logger.warning(
            "Can't find message body. Message\n{!r}".format(email_string))
        raise Exception("Can't find message body")

    return EmailData(subject=subject, from_addr=from_addr, sender=sender, body=body, uid=None)
