import pytest
from metric.ftp_importer import TopImporter
import os


basepath = '/top'
importer_class = TopImporter


@pytest.yield_fixture
def rest_xml():
    path = os.path.join(
        os.path.dirname(__file__),
        'rest/rest1.xml'
    )
    with open(path, 'rb') as f:
        yield f.read()


@pytest.mark.parametrize('num, attr, val', [
    (0, 'Article', 17927),
    (0, 'Name', '10 Монастырей Москвы.'),
    (0, 'UoM', 'шт'),
    (0, 'ImageAddress', 'http://blagovest.ru/img.html'),
    (0, 'Rest', 8),
    (0, 'FlagDiscountOff', 1),
    (0, 'FlagDiscountDis', 0),
    (1, 'Article', 17928),
    (1, 'Name', '10 Монастырей Москвы. часть 2'),
    (1, 'UoM', 'шт'),
    (1, 'ImageAddress', 'http://blagovest.ru/img2.html'),
    (1, 'Rest', 10),
    (1, 'FlagDiscountOff', 0),
    (1, 'FlagDiscountDis', 1),
])
def test_load_data_from_xml(importer, rest_xml, num, attr, val):
    data = importer._parse_xml(rest_xml)
    assert val == data[num][attr]
