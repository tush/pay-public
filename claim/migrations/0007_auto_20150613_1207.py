# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('claim', '0006_auto_20150612_2359'),
    ]

    operations = [
        migrations.AlterField(
            model_name='claim',
            name='delivery_method',
            field=models.CharField(verbose_name='Способ доставки', max_length=20, choices=[('post', 'Почтой'), ('courier', 'Курьером'), ('self', 'Самовывоз'), ('ems', 'Экспресс доставка через EMS почту'), ('transport', 'Транспортная компания'), ('courier_russia', 'Курьерская доставка в города России'), ('self_proletarskaya', 'Самовывоз Пролетарская'), ('self_strogino', 'Самовывоз Строгино'), ('self_schukinskaya', 'Самовывоз Щукинская')], blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='claim',
            name='message',
            field=models.TextField(verbose_name='Сообщение', blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='claim',
            name='pay_form',
            field=models.CharField(verbose_name='Способ оплаты', max_length=50, choices=[('cash', 'Наличными при получении'), ('bank', 'Оплата через банк'), ('emoney', 'Электронные деньги'), ('other', 'Другой способ оплаты')], blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='claim',
            name='subject',
            field=models.CharField(verbose_name='Тема', max_length=255, blank=True, null=True),
        ),
    ]
