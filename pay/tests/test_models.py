
from decimal import Decimal
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.core.files import File
from django.test import TestCase
from django.test.utils import override_settings
from pay.models import PayInfo, BankFile, BankRecord, EmailInfo

import os.path as p

from unittest.mock import patch
from hamcrest.library.text.isequal_ignoring_whitespace import equal_to_ignoring_whitespace
from hamcrest import assert_that, equal_to
import pytest


@override_settings(EXPORT_PATH='/tmp/pay-test')
class PayInfoTest(TestCase):

    def test_create(self):
        pi = PayInfo.objects.create(
            summ=Decimal('123456.34'),
            date=timezone.now().date(),
            source=PayInfo.SOURCE_WESTERN_UNION,
            comment="Простой комментарий"
        )
        self.assertIsNotNone(pi.pk)
        self.assertEqual(pi.summ, Decimal('123456.34'))
        self.assertEqual(pi.date, timezone.now().date())
        self.assertEqual(pi.source, PayInfo.SOURCE_WESTERN_UNION)
        self.assertEqual(pi.comment, "Простой комментарий")

    def test_export(self):
        pi = PayInfo(
            id=123,
            summ=Decimal('123456.34'),
            date=timezone.datetime(2015, 2, 17).date(),
            source=PayInfo.SOURCE_WESTERN_UNION,
            comment="Простой комментарий"
        )
        pi.export()
        assert p.exists('/tmp/pay-test/123.xml')
        with open('/tmp/pay-test/123.xml', 'rb') as f:
            assert_that(f.read().decode('cp1251'), equal_to_ignoring_whitespace(
                """
                <?xml version='1.0' encoding='windows-1251'?>
                <Данные xmlns:dt="urn:schemas-microsoft-com:datatypes" xmlns="urn:upload_schema">
                  <ОбъектыМДТипа_Документ>
                    <Документ_Оплата ДатаДок="17.02.15" Источник="Western Union" Комментарий="Western Union: Простой комментарий" НомерДок="123" Сумма="123456.34"></Документ_Оплата>
                  </ОбъектыМДТипа_Документ>
                </Данные>
                """
            ))

    def test_call_export_on_save(self):
        pi = PayInfo(
            id=123,
            summ=Decimal('123456.34'),
            date=timezone.now().date(),
            source=PayInfo.SOURCE_WESTERN_UNION,
            comment="Простой комментарий"
        )
        with patch.object(pi, 'export') as export_mock:
            pi.save()
        assert 1 == export_mock.call_count

    def test_validate_summ(self):
        with self.assertRaises(ValidationError):
            PayInfo.objects.create(
                date=timezone.now().date(),
                source=PayInfo.SOURCE_WESTERN_UNION,
            )

    def test_validate_date(self):
        with self.assertRaises(ValidationError):
            PayInfo.objects.create(
                summ=Decimal('123456.34'),
                date=None,
                source=PayInfo.SOURCE_WESTERN_UNION,
            )

    def test_validate_source(self):
        with self.assertRaises(ValidationError):
            PayInfo.objects.create(
                summ=Decimal('123456.34'),
                date=timezone.now().date(),
                source='ot',
            )

    def test_set_email_id(self):
        pi = PayInfo.objects.create(
            summ=Decimal('123456.34'),
            source=PayInfo.SOURCE_WESTERN_UNION,
            email_id=12355
        )
        self.assertEqual(pi.email_id, 12355)

    @override_settings(XML_FOLDER='/tmp/qqq')
    def test_save_file(self):
        pass


@pytest.mark.django_db
def test_override_default_date():
    date = timezone.datetime(2015, 2, 17).date()
    pi = PayInfo.objects.create(
        id=123,
        summ=Decimal('123456.34'),
        date=date,
        source=PayInfo.SOURCE_WESTERN_UNION,
        comment="Простой комментарий"
    )
    pi = PayInfo.objects.get(pk=pi.pk)
    assert_that(pi.date, equal_to(date))


class EmailInfoTest(TestCase):

    def setUp(self):
        super(EmailInfoTest, self).setUp()
        self.email_dir = p.join(p.dirname(__file__), 'emails')

    def test_load_message(self):
        with open(p.join(self.email_dir, 'yandex1.eml')) as f:
            email = f.read()

        ei = EmailInfo.create_from_string(5, email, timezone.now())
        assert ei.pk is not None
        assert 'Перевод от другого пользователя' in ei.body
        assert 'inform@money.yandex.ru' == ei.sender

    def test_parse_yandex1(self):
        with open(p.join(self.email_dir, 'yandex1.eml')) as f:
            email = f.read()

        ei = EmailInfo.create_from_string(5, email, timezone.now())
        assert 1613 == ei.get_summ()
        comment = ei.get_comment()
        assert 'Комментарий: Заказ №136220, Локтева Анна Александровна' in comment

    def test_parse_yandex2(self):
        with open(p.join(self.email_dir, 'yandex2.eml')) as f:
            email = f.read()

        ei = EmailInfo.create_from_string(5, email, timezone.now())
        assert 831 == ei.get_summ()
        comment = ei.get_comment()
        assert 'Пополнение через: Сбербанк РФ (МСК), пополнение' in comment

    def test_parse_yandex3(self):
        with open(p.join(self.email_dir, 'yandex3.eml')) as f:
            email = f.read()

        ei = EmailInfo.create_from_string(5, email, timezone.now())
        assert 1015 == ei.get_summ()
        comment = ei.get_comment()
        assert 'Комментарий: Заказ 142075, Коваленко' in comment

    def test_parse_yandex4(self):
        with open(p.join(self.email_dir, 'yandex4.eml')) as f:
            email = f.read()

        ei = EmailInfo.create_from_string(5, email, timezone.now())
        assert 925 == ei.get_summ()
        comment = ei.get_comment()
        assert '' == comment

    def test_parse_rbc(self):
        with open(p.join(self.email_dir, 'rbk.eml')) as f:
            email = f.read()

        ei = EmailInfo.create_from_string(5, email, timezone.now())
        assert Decimal('13080.00') == ei.get_summ()
        comment = ei.get_comment()
        assert 'платеж №2080081029' in comment
        assert 'Email покупателя: erysenko@yandex.ru' in comment
        assert 'Номер заказа (OrderId): 138707' in comment

    def test_parse_rbc2(self):
        """Уведомление о готовящемся платеже"""
        with open(p.join(self.email_dir, 'rbk2.eml')) as f:
            email = f.read()

        ei = EmailInfo.create_from_string(5, email, timezone.now())
        with self.assertRaises(Exception):
            ei.get_summ()


class BankFileTest(TestCase):

    def test_create(self):
        f = open(p.join(p.dirname(__file__), 'emails', 'bank.txt'), 'rb')
        b = BankFile.objects.create(
            f=File(f)
        )
        self.assertIsNotNone(b.pk)
        self.assertIsNotNone(b.f)

    def test_parse_file(self):
        f = open(p.join(p.dirname(__file__), 'emails', 'bank.txt'), 'rb')
        b = BankFile.objects.create(
            f=File(f)
        )
        self.assertEqual(9, b.records.count())


class BankRecordTest(TestCase):

    def test_create(self):
        now = timezone.now().date()
        br = BankRecord.objects.create(
            summ=Decimal('123456.78'),
            date=now,
            payer='Заказчик',
            comment=u'Оплата книги',
            bank_file_id=1
        )
        self.assertIsNotNone(br.pk)
        br = BankRecord.objects.get(pk=br.pk)
        self.assertEqual(br.summ, Decimal('123456.78'))
        self.assertEqual(br.date, now)
        self.assertEqual(br.payer, 'Заказчик')
        self.assertEqual(br.comment, 'Оплата книги')
