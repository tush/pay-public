
from lxml import etree
from decimal import Decimal
from django.utils import timezone


class XmlBuilder(object):

    def __init__(self):
        self._root = etree.Element('Данные', nsmap={
            None: 'urn:upload_schema',
            'dt': "urn:schemas-microsoft-com:datatypes"
        })
        self.date = timezone.now()
        self.id = None
        self.source = ''
        self.summ = Decimal('0')
        self.comment = ''

    def __bytes__(self):
        comment = self.comment if self.comment is not None else ''
        comment = comment.replace('\n', ' ')
        doc_pay = etree.Element('Документ_Оплата', {
            'ДатаДок': self.date.strftime('%d.%m.%y'),
            'НомерДок': repr(self.id),
            'Источник': self.source,
            'Сумма': str(self.summ),
            'Комментарий': comment,
        })
        doc_pay.text = ''
        obj_doc = etree.Element('ОбъектыМДТипа_Документ')
        self._root.append(obj_doc)
        obj_doc.append(doc_pay)
        return etree.tostring(
            self._root,
            encoding='windows-1251',
            pretty_print=True)
