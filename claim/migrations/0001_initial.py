# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Claim',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('sender', models.CharField(max_length=255, verbose_name='Отправитель')),
                ('subject', models.CharField(max_length=255, verbose_name='Тема')),
                ('message', models.TextField(verbose_name='Сообщение')),
                ('comment', models.TextField(null=True, verbose_name='Комментарий оператора', blank=True)),
                ('order_num', models.IntegerField(null=True, verbose_name='Номер заказа', blank=True)),
                ('delivery_method', models.CharField(choices=[('post', 'Почтой'), ('courier', 'Курьером'), ('self', 'Самовывоз'), ('ems', 'Экспресс доставка через EMS почту'), ('transport', 'Транспортная компания'), ('courier_russia', 'Курьерская доставка в города России'), ('self_proletarskaya', 'Самовывоз Пролетарская'), ('self_strogino', 'Самовывоз Строгино'), ('self_schukinskaya', 'Самовывоз Щукинская')], max_length=20, verbose_name='Способ доставки')),
                ('pay_form', models.CharField(choices=[('cash', 'Наличными при получении'), ('bank', 'Оплата через банк'), ('emoney', 'Электронные деньги'), ('other', 'Другой способ оплаты')], max_length=50, verbose_name='Способ оплаты')),
                ('manager', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'default_permissions': (),
                'permissions': (('can_work_with_claims', 'Может работать с обращениями'),),
                'verbose_name': 'Claim',
                'verbose_name_plural': 'Claims',
            },
        ),
    ]
