from django.db import models
from metric.yandex import Metrika
from django.conf import settings

from .ftp_importer import CrmImporter, TopImporter
from .mixins import FtpUpdaterMixin, UpdateAllMixin


class NotConfigured(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "Ошибка настройки: {}".format(self.value)


class Config(models.Model):
    metrika_token = models.CharField('Токен Яндекс Метрики', max_length=255)
    metrika_counter_id = models.CharField('Id счетчика Яндекс Метрики', max_length=32)

    class Meta:
        default_permissions = ()

    @classmethod
    def get(cls):
        instance = cls.objects.last()
        if instance is None:
            instance = cls()
        return instance

    @classmethod
    def set(cls, **kwargs):
        instance = cls.objects.last()
        if instance is None:
            instance = cls()
        instance.__dict__.update(kwargs)
        instance.save()


class DateData(models.Model):
    date = models.DateField('Дата', unique=True)

    class Meta:
        abstract = True
        ordering = ['date']
        default_permissions = ()

    @classmethod
    def get_updater(cls):
        raise NotImplementedError()

    @classmethod
    def update(cls, date):
        raise NotImplementedError()


class MetrikaData(DateData):
    visitors = models.IntegerField('Посетители')
    page_views = models.IntegerField('Просмотры')
    denial = models.FloatField('Отказы')
    visit_time = models.IntegerField('Время на сайте')

    class Meta:
        default_permissions = ()
        permissions = (
            ('can_view_stat', 'Может просматривать статистику'),
        )

    @classmethod
    def get_updater(cls):
        config = Config.get()
        if config.id is None:
            raise NotConfigured("Токен для метрики не задан.")
        token = config.metrika_token
        return Metrika(token)

    @classmethod
    def update(cls, date):
        updater = cls.get_updater()
        config = Config.get()
        if config.id is None:
            raise NotConfigured("Не указан номер счетчика метрики.")
        counter_id = config.metrika_counter_id
        day_data = updater.get_day_data(counter_id, date)
        try:
            instance = cls.objects.get(date=date)
        except cls.DoesNotExist:
            instance = cls(date=date)
        fields = dict(
            visitors=day_data['visitors'],
            page_views=day_data['page_views'],
            denial=day_data['denial'],
            visit_time=day_data['visit_time'],
        )
        instance.__dict__.update(fields)
        instance.save()
        return instance


class CrmData(FtpUpdaterMixin, UpdateAllMixin, DateData):
    order_count = models.IntegerField("Количество заказов")
    sell_count = models.IntegerField("Количество реализаций")
    return_count = models.IntegerField("Количество возвратов")
    order_summ = models.DecimalField("Сумма заказов", max_digits=15, decimal_places=2)
    sell_summ = models.DecimalField("Сумма реализаций", max_digits=15, decimal_places=2)
    return_summ = models.DecimalField("Сумма возвратов", max_digits=15, decimal_places=2)

    updater = None
    updater_class = CrmImporter
    ftp_folder = settings.FTP_DASHBOARD_FOLDER

    @classmethod
    def update(cls, date):
        updater = cls.get_updater()
        data = updater.get_data(date)
        try:
            instance = cls.objects.get(date=date)
        except cls.DoesNotExist:
            instance = cls(date=date)
        fields = dict(
            order_count=data['order']['count'],
            sell_count=data['sell']['count'],
            return_count=data['return']['count'],
            order_summ=data['order']['summ'],
            sell_summ=data['sell']['summ'],
            return_summ=data['return']['summ'],
        )
        instance.__dict__.update(fields)
        instance.save()
        return instance


class Rest(models.Model, UpdateAllMixin, FtpUpdaterMixin):
    date = models.DateField('Дата')
    rest = models.IntegerField('Остаток')
    article = models.CharField('Артикул', max_length=64)
    name = models.TextField('Название')
    uom = models.CharField('Единица измерения', max_length=32)
    imageaddress = models.URLField('Адрес', max_length=1024)
    flagdiscountoff = models.BooleanField('*')
    flagdiscountdis = models.BooleanField('!')

    updater = None
    updater_class = TopImporter
    ftp_folder = settings.FTP_TOP_FOLDER

    class Meta:
        default_permissions = ()
        permissions = (
            ('can_view_top', 'Может просматривать ТОП'),
        )

    @classmethod
    def update(cls, date):
        updater = cls.get_updater()
        cls.objects.filter(date=date).delete()
        data = updater.get_data(date)
        for item in data:
            # Keys to lowercase
            item = {k.lower(): v for k, v in item.items()}
            instance = cls(date=date)
            instance.__dict__.update(item)
            instance.save()
