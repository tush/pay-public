from django.apps import AppConfig


class ClaimAppConfig(AppConfig):
    name = 'claim'
    verbose_name = 'Обращения'
