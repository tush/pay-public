from pay.ftp import FTPClient


class FtpUpdaterMixin(object):

    @classmethod
    def get_updater(cls):
        if cls.updater is None:
            ftp_client = FTPClient()
            cls.updater = cls.updater_class(ftp_client, cls.ftp_folder)
        return cls.updater


class UpdateAllMixin(object):

    @classmethod
    def update_all(cls):
        updater = cls.get_updater()
        dates = updater.scan_dates()
        exists_dates = cls.objects.values_list('date', flat=True)
        for date in dates:
            if date in exists_dates:
                continue
            cls.update(date)
