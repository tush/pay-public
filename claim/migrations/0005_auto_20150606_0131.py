# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('claim', '0004_auto_20150606_0124'),
    ]

    operations = [
        migrations.AlterField(
            model_name='claim',
            name='subject',
            field=models.CharField(max_length=255, verbose_name='Тема', null=True),
        ),
    ]
