from __future__ import absolute_import

from celery import shared_task


@shared_task
def check_mails():
    from .models import Claim
    Claim.update_from_email()


@shared_task(bind=True, max_retries=3, default_retry_delay=30)
def export_claim(self, content, filename):
    try:
        from .models import ClaimFtpExporter
        exporter = ClaimFtpExporter()
        exporter.export(filename, content)
    except Exception as exc:
            raise self.retry(exc=exc)
