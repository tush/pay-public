# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('claim', '0008_auto_20150613_2314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='claim',
            name='is_spam',
            field=models.BooleanField(verbose_name='Спам', default=False, db_index=True),
        ),
    ]
