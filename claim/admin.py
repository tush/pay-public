from django.contrib import admin
from .models import Claim


@admin.register(Claim)
class ClaimAdmin(admin.ModelAdmin):
    date_hierarchy = 'sended_on'
    list_display = ('id', 'sender', 'subject', 'sended_on')
