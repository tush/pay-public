
import pytest
import datetime
from metric.models import Rest
from unittest.mock import Mock, patch


@pytest.fixture
def updater_class():
    return Mock()


@pytest.fixture
def RestClean(updater_class):
    Rest.updater = None
    Rest.ftp_folder = "ftp_folder"
    Rest.updater_class = updater_class
    return Rest


@pytest.mark.django_db
def test_record_create():
    date = datetime.datetime(2014, 12, 15).date()
    t = Rest(
        date=date,
        article=17927,
        name='10 Монастырей Москвы.',
        uom='шт',
        imageaddress='http://blagovest.ru/img.html',
        rest=15,
        flagdiscountoff=True,
        flagdiscountdis=False,
    )
    t.save()
    assert t.id is not None
    assert t.date == date


@patch('metric.mixins.FTPClient')
def test_get_updater(ftp_class_mock, RestClean, updater_class):
    updater_mock = updater_class.return_value
    updater = RestClean.get_updater()
    assert updater_mock == updater
    updater_class.assert_called_once_with(ftp_class_mock.return_value, "ftp_folder")


def test_updater_created_once(RestClean, updater_class):
    updater = RestClean.get_updater()
    updater2 = RestClean.get_updater()
    assert updater2 == updater
    updater_class.assert_called_once()


@pytest.mark.django_db
def test_update_day_data_call_updater(RestClean, updater_class):
    date = datetime.datetime(2014, 12, 15).date()
    RestClean.objects.create(
        date=date,
        article=17927,
        name='10 Монастырей Москвы.',
        uom='шт',
        imageaddress='http://blagovest.ru/img.html',
        rest=15,
        flagdiscountoff=True,
        flagdiscountdis=False,
    )
    gd = updater_class.return_value.get_data
    gd.return_value = []
    RestClean.update(date=date)
    gd.assert_called_once_with(date)


@pytest.mark.django_db
def test_update_day_data_clean_old(RestClean, updater_class):
    date = datetime.datetime(2014, 12, 15).date()
    RestClean.objects.create(
        date=date,
        article=17927,
        name='10 Монастырей Москвы.',
        uom='шт',
        imageaddress='http://blagovest.ru/img.html',
        rest=15,
        flagdiscountoff=True,
        flagdiscountdis=False,
    )
    gd = updater_class.return_value.get_data
    gd.return_value = []
    RestClean.update(date=date)
    assert 0 == RestClean.objects.count()


@pytest.mark.django_db
def test_update_save_data(RestClean, updater_class):
    date = datetime.datetime(2014, 12, 15).date()
    gd = updater_class.return_value.get_data
    gd.return_value = [
        {
            'Article': 17927,
            'Name': '10 Монастырей Москвы.',
            'UoM': 'шт',
            'ImageAddress': 'http://blagovest.ru/img.html',
            'Rest': 8,
            'FlagDiscountOff': 1,
            'FlagDiscountDis': 0,
        },
        {
            'Article': 17928,
            'Name': '10 Монастырей Москвы. часть 2',
            'UoM': 'шт',
            'ImageAddress': 'http://blagovest.ru/img2.html',
            'Rest': 10,
            'FlagDiscountOff': 0,
            'FlagDiscountDis': 1,
        },
    ]
    RestClean.update(date=date)
    assert 2 == RestClean.objects.count()
