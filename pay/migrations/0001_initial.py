# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BankFile',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('f', models.FileField(upload_to='.', verbose_name='Файл')),
            ],
            options={
                'verbose_name_plural': 'BankFiles',
                'default_permissions': (),
                'verbose_name': 'BankFile',
            },
        ),
        migrations.CreateModel(
            name='BankRecord',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('summ', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Сумма')),
                ('date', models.DateField(default=django.utils.timezone.now, verbose_name='Дата')),
                ('payer', models.CharField(max_length=1024, verbose_name='Плательщик')),
                ('comment', models.TextField(null=True, blank=True, verbose_name='Комментарий')),
                ('bank_file', models.ForeignKey(related_name='records', verbose_name='Запись файла', to='pay.BankFile')),
            ],
            options={
                'verbose_name_plural': 'BankRecords',
                'default_permissions': (),
                'verbose_name': 'BankRecord',
            },
        ),
        migrations.CreateModel(
            name='EmailInfo',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('email_id', models.IntegerField(unique=True)),
                ('sender', models.EmailField(max_length=254)),
                ('subject', models.CharField(max_length=1024)),
                ('date', models.DateField()),
                ('body', models.TextField()),
            ],
            options={
                'verbose_name_plural': 'EmailInfos',
                'default_permissions': (),
                'verbose_name': 'EmailInfo',
            },
        ),
        migrations.CreateModel(
            name='PayInfo',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('email_id', models.IntegerField(null=True, blank=True, verbose_name='ID письма')),
                ('summ', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Сумма')),
                ('date', models.DateField(default=django.utils.timezone.now, verbose_name='Дата')),
                ('source', models.CharField(max_length=2, default='o', choices=[('b', 'Банк'), ('y', 'Яндекс.Деньги'), ('r', 'РБК-мани'), ('wm', 'Webmoney'), ('wu', 'Western Union'), ('q', 'QIWI'), ('c', 'Наличные'), ('o', 'Другое')], verbose_name='Источник')),
                ('comment', models.TextField(null=True, blank=True, verbose_name='Комментарий')),
            ],
            options={
                'verbose_name_plural': 'PayInfos',
                'permissions': (('view_payinfo', 'Может просматривать оплаты'),),
                'default_permissions': (),
                'verbose_name': 'PayInfo',
            },
        ),
    ]
