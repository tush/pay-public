# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('claim', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='claim',
            options={'permissions': (('can_work_with_claims', 'Может работать с обращениями'),), 'verbose_name_plural': 'Claims', 'verbose_name': 'Claim', 'default_permissions': (), 'ordering': ['-sended_on']},
        ),
        migrations.AddField(
            model_name='claim',
            name='sended_on',
            field=models.DateTimeField(verbose_name='Дата отправки', default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='claim',
            name='manager',
            field=models.ForeignKey(verbose_name='Менеджер', to=settings.AUTH_USER_MODEL),
        ),
    ]
