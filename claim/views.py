from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.base import RedirectView
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from urllib import parse
from .models import Claim


class ExportMixin(object):

    def form_valid(self, form):
        form.instance.manager = self.request.user
        form.instance.exported = True
        form.instance.seen = True
        form.instance.save()
        form.instance.export()
        messages.success(self.request, 'Запись "{0.subject}" выгружена'.format(form.instance))
        return super(ExportMixin, self).form_valid(form)


class ClaimCreate(ExportMixin, CreateView):
    template_name = "claim/form.html"
    model = Claim
    fields = [
        "sender",
        "subject",
        "message",
        "comment",
        "order_num",
        "delivery_method",
        "pay_form",
    ]
    success_url = reverse_lazy('claim-list')


class ClaimUpdate(ExportMixin, UpdateView):
    template_name = "claim/form.html"
    model = Claim
    fields = [
        "sender",
        "subject",
        "message",
        "comment",
        "order_num",
        "delivery_method",
        "pay_form",
    ]

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        next_url = self.request.GET.get('next')
        multi_action = True
        if next_url is None:
            multi_action = False
            next_url = self.request.META.get('HTTP_REFERER')
        context['next'] = parse.quote(next_url)
        context['multi_action'] = multi_action
        return context

    def get_success_url(self, *args, **kwargs):
        return self.request.GET.get('next', reverse_lazy('claim-list'))


class ClaimSpam(SingleObjectMixin, RedirectView):
    model = Claim
    url = reverse_lazy('claim-list')
    permanent = False

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.is_spam = True
        self.object.save()
        messages.success(request, 'Запись "{0.subject}" помечена как спам'.format(self.object))
        return super(ClaimSpam, self).get(request, *args, **kwargs)


class ManyClaimsAction(RedirectView):
    permanent = False
    pattern_name = 'claim-list'
    query_string = True
    ids_get_param = "ids"
    fin_url_get_param = "fin"

    def get_pks(self):
        return self.request.GET.getlist(self.ids_get_param)

    def get_fin_url(self):
        fin_url = self.request.GET.get(self.fin_url_get_param)
        if fin_url is None:
            fin_url = self.request.META.get('HTTP_REFERER')
        return fin_url

    def process_spam(self, *args, **kwargs):
        pks = self.get_pks()
        fin_url = self.get_fin_url()
        count = Claim.objects.filter(pk__in=pks).update(is_spam=True)
        messages.success(self.request, '{} записей помечено как спам'.format(count))
        return fin_url

    def process_export(self, *args, **kwargs):
        pks = self.get_pks()
        fin_url = self.get_fin_url()
        if len(pks) > 0:
            pk = pks.pop(0)
            url = reverse_lazy('edit-claim', kwargs={'pk': pk})
            next_url = reverse_lazy('multi-action')
            next_url += '?' + parse.urlencode({
                self.ids_get_param: pks,
                self.fin_url_get_param: fin_url
            }, doseq=True)
            return "{}?{}".format(url, parse.urlencode({'next': next_url}))
        else:
            return fin_url

    def get_redirect_url(self, *args, **kwargs):
        action = self.request.GET.get('action', 'export')
        method = getattr(self, 'process_{}'.format(action))
        return method(*args, **kwargs)


class ClaimList(ListView):
    queryset = Claim.objects.exclude(is_spam=True)
    template_name = "claim/list.html"
    paginate_by = 25
