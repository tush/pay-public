from django.test import TestCase
from pay.ftp import FTPClient
import pytest


class FTPClientTest(TestCase):

    def setUp(self):
        super(FTPClientTest, self).setUp()
        self.c = FTPClient()

    def test_connect(self):
        self.assertIsNotNone(self.c.dir())

    def test_dir(self):
        files = self.c.dir('private/PAY')
        assert len(files) > 0
        assert '/' not in files[0]

    def test_exists(self):
        self.c.mkdir('private/PAY_TEST')
        self.c.mkdir('private/PAY_TEST1/test1')
        self.assertTrue(self.c.exists('private/PAY_TEST'))
        self.assertTrue(self.c.exists('private/PAY_TEST1/test1'))
        self.assertFalse(self.c.exists('private/PAY_TEST/q.xml'))

    def test_mkdir(self):
        self.c.mkdir('private/PAY_TEST')
        self.assertIn('PAY_TEST', self.c.dir('private'))

    def test_save_file(self):
        self.c.mkdir('private/PAY_TEST2')
        self.c.store('private/PAY_TEST2/q.xml', b'Hello world')
        self.assertTrue(self.c.exists('private/PAY_TEST2/q.xml'))

    def test_get_file(self):
        self.c.store('private/PAY_TEST2/q.xml', b'Hello world')
        content = self.c.get('private/PAY_TEST2/q.xml')
        assert b'Hello world' == content.read()

    def test_get_not_exists_file(self):
        with pytest.raises(IOError):
            self.c.get('private/PAY_TEST2/not_exists.xml')
