from django.db.models import Sum, F
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import View
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView, FormMixin
import datetime
import csv
from io import StringIO
from decimal import Decimal
import json
from .forms import DateIntervalForm
from .models import Config, MetrikaData, CrmData, Rest
from .yandex import Metrika


class ModelLabelContextView(object):

    def get_context_data(self, **kwargs):
        context = super(ModelLabelContextView, self).get_context_data(**kwargs)
        metric_labels = {}
        for field in MetrikaData._meta.fields:
            metric_labels[field.name] = field.verbose_name
        context['metric_labels'] = metric_labels
        return context


class ConfigView(UpdateView):
    template_name_suffix = '_form'
    model = Config
    fields = ['metrika_counter_id']
    success_url = '/'

    def get_success_url(self):
        m = Metrika()
        return m.confirm_code_url

    def get_object(self):
        obj = Config.get()
        return obj

    def form_valid(self, form):
        return redirect(self.get_success_url())


class YandexVerificationView(View):

    def get(self, request, *args, **kwargs):
        code = request.GET.get('code')
        if code is not None:
            m = Metrika()
            token = m.get_token(code)
            Config.set(metrika_token=token)
        return redirect('/')


class DashboardView(ModelLabelContextView, FormMixin, TemplateView):
    template_name = "metric/dashboard.html"
    form_class = DateIntervalForm

    def get_initial(self):
        form_class = self.get_form_class()
        form = form_class(self.request.GET)
        if form.is_valid():
            return form.cleaned_data
        else:
            date_to = datetime.date.today()
            date_from = date_to - datetime.timedelta(30)
            return {
                'date_to': date_to,
                'date_from': date_from,
            }

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        form_class = self.get_form_class()
        context['form'] = self.get_form(form_class)
        context['date_from'] = self.get_initial()['date_from'].strftime('%Y%m%d')
        context['date_to'] = self.get_initial()['date_to'].strftime('%Y%m%d')
        context['data_types'] = ['order', 'sell', 'return']
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return '?date_from={}&date_to={}'.format(
            self.date_from.strftime('%Y%m%d'),
            self.date_to.strftime('%Y%m%d')
        )

    def form_valid(self, form):
        data = form.cleaned_data
        self.date_from = data['date_from']
        self.date_to = data['date_to']
        return super(DashboardView, self).form_valid(form)


class BaseCsvView(View):

    @staticmethod
    def process_value(val):
        if val is None:
            return 0
        if isinstance(val, (float, Decimal)):
            return round(val, 2)
        return val

    def get_data(self, date1, date2, **kwargs):
        raise NotImplemented()

    def get_headers(self):
        raise NotImplemented()

    def get(self, request, date1, date2, **kwargs):
        date1 = datetime.datetime.strptime(date1, '%Y%m%d')
        date2 = datetime.datetime.strptime(date2, '%Y%m%d')
        buf = StringIO()
        data = self.get_data(date1, date2, **kwargs)
        writer = csv.writer(buf, dialect=csv.unix_dialect, quoting=csv.QUOTE_MINIMAL)
        writer.writerow(self.get_headers())
        for row in data:
            row = map(self.process_value, row)
            writer.writerow(list(row))
        buf.seek(0)
        return HttpResponse(buf.read(), "text/csv")


class MetricaCsvView(BaseCsvView):

    def get_data(self, date1, date2):
        keys = (
            'date',
            'visitors',
            'page_views',
            'denial_percent',
            'visit_time',
        )
        return MetrikaData.objects.filter(date__gte=date1, date__lte=date2).exclude(
            visitors=0
        ).extra(select={
            'denial_percent': 'SELECT denial * 100'
        }).values_list(*keys)

    def get_headers(self):
        return ['Дата', 'Посетители', 'Просмотры', '% отказов', 'Время на сайте']


class DinamicCsvView(BaseCsvView):

    def get_data(self, date1, date2):
        return CrmData.objects.filter(date__gte=date1, date__lte=date2).extra(select={
            'visitors': 'SELECT visitors FROM {metrika_table} WHERE {crm_table}.date={metrika_table}.date'.format(
                    crm_table=CrmData._meta.db_table,
                    metrika_table=MetrikaData._meta.db_table,
                )
            }).values_list('date', 'visitors', 'order_count').exclude(order_count=0)

    def get_headers(self):
        return ['Дата', 'Посетители', 'Заказы']


class CrmCsvView(BaseCsvView):

    def get_data(self, date1, date2, **kwargs):
        data_type = kwargs.get('data_type')
        if data_type is not None:
            return getattr(self, 'get_data_' + data_type)(date1, date2)
        else:
            return []

    def get_data_order(self, date1, date2):
        keys = (
            'date',
            'order_count',
            'order_summ',
            'order_cost',
        )
        return CrmData.objects.filter(date__gte=date1, date__lte=date2).exclude(order_count=0).extra(
            select={'order_cost': 'order_summ / order_count'}
        ).values_list(*keys)

    def get_data_sell(self, date1, date2):
        keys = (
            'date',
            'sell_count',
            'sell_summ',
            'sell_cost',
        )
        return CrmData.objects.filter(date__gte=date1, date__lte=date2).exclude(sell_count=0).extra(
            select={'sell_cost': 'sell_summ / sell_count'}
        ).values_list(*keys)

    def get_data_return(self, date1, date2):
        keys = (
            'date',
            'return_count',
            'return_summ',
            'return_cost',
        )
        return CrmData.objects.filter(date__gte=date1, date__lte=date2).exclude(return_count=0).extra(
            select={'return_cost': 'return_summ / return_count'}
        ).values_list(*keys)

    def get_headers(self):
        return ['Дата', 'Количество', 'Сумма', 'Среднее']


class TopView(ModelLabelContextView, FormMixin, TemplateView):
    template_name = "metric/top.html"
    form_class = DateIntervalForm

    def get_initial(self):
        form_class = self.get_form_class()
        form = form_class(self.request.GET)
        if form.is_valid():
            return form.cleaned_data
        else:
            date_to = datetime.date.today()
            date_from = date_to - datetime.timedelta(30)
            return {
                'date_to': date_to,
                'date_from': date_from,
            }

    def get_context_data(self, **kwargs):
        context = super(TopView, self).get_context_data(**kwargs)
        form_class = self.get_form_class()
        context['form'] = self.get_form(form_class)
        context['date_from'] = self.get_initial()['date_from'].strftime('%Y%m%d')
        context['date_to'] = self.get_initial()['date_to'].strftime('%Y%m%d')
        last_date_data = []
        last_record = Rest.objects.order_by('-date').last()
        if last_record:
            last_date_data = Rest.objects.filter(date=last_record.date).values(
                'id',
                'last_date',
                'rest',
                'flagdiscountdis',
                'flagdiscountoff',
                'article',
                'name',
                'imageaddress',
            )
        context['last_date_data'] = json.dumps(list(last_date_data))
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return '/?date_from={}&date_to={}'.format(
            self.date_from.strftime('%Y%m%d'),
            self.date_to.strftime('%Y%m%d')
        )

    def form_valid(self, form):
        data = form.cleaned_data
        self.date_from = data['date_from']
        self.date_to = data['date_to']
        return super(TopView, self).form_valid(form)


class TopCsvView(BaseCsvView):

    def get_data(self, date1, date2):
        return Rest.objects.filter(
            date__gte=date1,
            date__lte=date2
        ).values('date').annotate(sum_rest=Sum('rest')).values_list('date', 'sum_rest')

    def get_headers(self):
        return ['Дата', 'Количество']
