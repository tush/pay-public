
from metric.models import MetrikaData, Config
from metric.models import NotConfigured
import datetime
from django.core.exceptions import ValidationError

import pytest
from unittest.mock import patch, Mock
from django_dynamic_fixture import G


@pytest.mark.django_db
def test_create_metrika_data():
    m = MetrikaData(
        date=datetime.date.today(),
        visitors=33,
        page_views=123,
        denial=0.12,
        visit_time=55,

    )
    m.save()
    assert m.id is not None


@pytest.mark.parametrize("key", (
    "date",
    "visitors",
    "page_views",
    "denial",
    "visit_time",
))
@pytest.mark.django_db
def test_required_args(key):
    required_args = dict(
        date=datetime.date.today(),
        visitors=33,
        page_views=123,
        denial=0.12,
        visit_time=55,
    )
    partial_args = required_args.copy()
    partial_args.pop(key)
    m = MetrikaData(**partial_args)
    pytest.raises(ValidationError, m.full_clean)


@pytest.mark.django_db
def test_get_updater():
    G(Config, metrika_token="234")
    updater_mock = Mock()
    with patch('metric.models.Metrika', return_value=updater_mock) as m_mock:
        updater = MetrikaData.get_updater()
    assert updater_mock == updater
    m_mock.assert_called_once_with("234")


@pytest.mark.django_db
def test_get_updater_not_configured():
    pytest.raises(NotConfigured, MetrikaData.get_updater)


@pytest.mark.django_db
def test_update_from_yandex():
    G(Config, metrika_token="123", metrika_counter_id="555")
    day_data = {
        'visitors': 33,
        'page_views': 234,
        'denial': 0.12,
        'visit_time': 22,
    }
    date = datetime.date.today()
    updater_mock = Mock(**{
        'get_day_data.return_value': day_data
    })
    with patch.object(MetrikaData, 'get_updater', return_value=updater_mock) as updater_getter:
        result = MetrikaData.update(date)
    assert isinstance(result, MetrikaData)
    assert date == result.date
    assert day_data['visitors'] == result.visitors
    assert day_data['page_views'] == result.page_views
    assert day_data['denial'] == result.denial
    assert day_data['visit_time'] == result.visit_time
    updater_getter.assert_called_once_with()
    updater_mock.get_day_data.assert_called_once_with("555", date)


@pytest.mark.django_db
def test_update_from_yandex_not_configured():
    date = datetime.date.today()
    with patch.object(MetrikaData, 'get_updater'):
        pytest.raises(NotConfigured, MetrikaData.update, (date,))
