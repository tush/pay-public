import os
import email
import email.header
import email.utils
from lxml import html
from pyparsing import Word, Suppress, nums, SkipTo, \
    Literal, ParseException, Keyword, alphas, ZeroOrMore, \
    White, QuotedString, OneOrMore, LineEnd, Group
from decimal import Decimal
from difflib import get_close_matches

from django.db import models
from django.dispatch import receiver
from django.utils import timezone
from django.conf import settings
import logging
from pay.xmlbuilder import XmlBuilder
from pay.ftp import FTPClient
from .imap_fetcher import parse_email
from .tasks import export_payinfo

logger = logging.getLogger(__name__)


class FileExporter(object):

    def export(self, filename, stream):
        if not os.path.exists(settings.EXPORT_PATH):
            os.mkdir(settings.EXPORT_PATH)
        path = os.path.join(settings.EXPORT_PATH, filename)
        with open(path, 'wb') as f:
            f.write(stream)


class FtpExporter(object):
    base_folder = settings.FTP_FOLDER

    def __init__(self):
        self.client = FTPClient()

    def export(self, filename, stream):
        path = os.path.join(self.base_folder, filename)
        self.client.store(path, stream)

Exporter = FtpExporter


class PayInfoQuerySet(models.QuerySet):

    def search(self, q):
        possible_sources = []
        for source, verbose_name in PayInfo.SOURCES:
            if q.lower() in verbose_name.lower():
                possible_sources += [source]
        return self.filter(
            models.Q(comment__icontains=q)
            | models.Q(source__in=possible_sources)
        )


class PayInfo(models.Model):
    SOURCE_BANK = 'b'
    SOURCE_YANDEX = 'y'
    SOURCE_RBC = 'r'
    SOURCE_WEBMONEY = 'wm'
    SOURCE_WESTERN_UNION = 'wu'
    SOURCE_QIWI = 'q'
    SOURCE_CASH = 'c'
    SOURCE_OTHER = 'o'

    SOURCES = (
        (SOURCE_BANK, 'Банк'),
        (SOURCE_YANDEX, 'Яндекс.Деньги'),
        (SOURCE_RBC, 'РБК-мани'),
        (SOURCE_WEBMONEY, 'Webmoney'),
        (SOURCE_WESTERN_UNION, 'Western Union'),
        (SOURCE_QIWI, 'QIWI'),
        (SOURCE_CASH, 'Наличные'),
        (SOURCE_OTHER, 'Другое'),
    )

    SOURCES_EMAILS = {
        'inform@money.yandex.ru': SOURCE_YANDEX,
        'noreply@rbkmoney.ru': SOURCE_RBC,
        'naumov@blagovest-moskva.ru': SOURCE_BANK,
    }

    objects = PayInfoQuerySet.as_manager()

    email_id = models.IntegerField(
        u'ID письма',
        null=True,
        blank=True)
    summ = models.DecimalField(u'Сумма', max_digits=10, decimal_places=2)
    date = models.DateField(u'Дата', default=timezone.now)
    source = models.CharField(
        u'Источник',
        max_length=2,
        default=SOURCE_OTHER,
        choices=SOURCES)
    comment = models.TextField(u'Комментарий', null=True, blank=True)

    class Meta:
        verbose_name = "PayInfo"
        verbose_name_plural = "PayInfos"
        permissions = (
            ("can_view_payinfo", "Может просматривать журнал платежей"),
            ("can_add_payinfo", "Может вносить оплаты"),
            ("can_del_payinfo", "Может удалять оплаты"),
        )
        default_permissions = ()

    def save(self, *args, **kwargs):
        self.full_clean()
        super(PayInfo, self).save(*args, **kwargs)
        self.export()

    def export(self):
        builder = XmlBuilder()
        builder.date = self.date
        builder.id = self.id
        builder.source = self.get_source_display()
        builder.summ = self.summ
        builder.comment = "{}: {}".format(self.get_source_display(), self.comment)
        filename = "{}.xml".format(self.id)
        export_payinfo.apply_async((bytes(builder), filename))


class EmailInfo(models.Model):

    SOURCES_EMAILS = PayInfo.SOURCES_EMAILS

    email_uid = models.IntegerField(unique=True, null=True)
    sender = models.EmailField()
    subject = models.CharField(max_length=1024)
    date = models.DateField()
    body = models.TextField()

    class Meta:
        verbose_name = "EmailInfo"
        verbose_name_plural = "EmailInfos"
        default_permissions = ()

    def __str__(self):
        return "Письмо {} от {}. Тема '{}'".format(self.email_uid, self.sender, self.subject)

    @classmethod
    def _flat_childs(cls, msg):
        if msg.get_content_maintype() == 'multipart':
            for part in msg.get_payload():
                yield from cls._flat_childs(part)

        else:
            yield msg

    @classmethod
    def create_from_string(cls, email_uid, email_string, date):

        email_data = parse_email(email_string)

        instance = cls.objects.create(
            email_uid=email_uid,
            subject=email_data.subject,
            body=email_data.body,
            sender=email_data.sender,
            date=date,
        )
        return instance

    def get_summ(self):
        body = self.body.replace('\xa0', ' ')
        source = PayInfo.SOURCES_EMAILS.get(self.sender)
        if source == PayInfo.SOURCE_YANDEX:
            money = Word(nums + ', ') + Suppress('руб.')
            template = Suppress(Keyword('Сумма') + SkipTo(money)) + money
        elif source == PayInfo.SOURCE_RBC:
            pay_label = Keyword('Зачислено')
            currency = Keyword('руб.')
            template = Suppress(
                pay_label + Word(' :')
            ) + Word(nums + ', ') + Suppress(currency)
        else:
            return None

        try:
            summ = template.searchString(body)[0][0]
        except IndexError:
            raise Exception('Не обработалось письмо {}'.format(self.email_uid))
        summ = Decimal(summ.replace(' ', '').replace(',', '.'))
        return summ

    def get_comment(self):
        body = self.body.replace('\xa0', ' ')
        doc = html.fromstring(body)
        for tr in doc.xpath('.//tr'):
            if tr.tail is not None:
                tr.tail += "\n"
            else:
                tr.tail = "\n"
        body = html.tostring(doc, method='text', encoding=str)
        source = PayInfo.SOURCES_EMAILS.get(self.sender)
        if source == PayInfo.SOURCE_YANDEX:
            comment_keyword = Keyword('Комментарий')
            pay_throught_keyword = Keyword('Пополнение через')
            template = (
                comment_keyword | pay_throught_keyword
            ) + SkipTo(LineEnd()).setParseAction(lambda s, l, t: t[0].strip())
            parts = template.searchString(body)
            if parts:
                return ': '.join(parts[0])
            else:
                return ''
        elif source == PayInfo.SOURCE_RBC:
            pay_num = Keyword('платеж') + Word(nums + '№') + Suppress('от')
            email = Keyword('Email покупателя:') + SkipTo(LineEnd())
            order_num = Keyword('Номер заказа') + SkipTo(LineEnd())
            template = (Group(pay_num) +
                        Suppress(SkipTo(email)) +
                        Group(email) +
                        Suppress(SkipTo(order_num)) +
                        Group(order_num))
            return '\n'.join([' '.join(x) for x in template.searchString(body)[0]])
        else:
            return None

    def save_pay(self):
        summ = self.get_summ()
        if summ is not None:
            PayInfo.objects.create(
                summ=summ,
                source=PayInfo.SOURCES_EMAILS[self.sender],
                date=self.date,
                comment=self.get_comment(),
            )


class BankFile(models.Model):

    f = models.FileField(u'Файл', upload_to=".")

    class Meta:
        verbose_name = "BankFile"
        verbose_name_plural = "BankFiles"
        default_permissions = ()

    def __str__(self):
        return "Файл от банка #{}".format(self.pk)

    def parse_file(self):
        content = self.f.read()
        content = content.decode('cp1251')
        content = content.strip()
        parts = content.split('СекцияДокумент=Платежное поручение')
        parts = filter(lambda x: 'Получатель1' in x, parts)
        summ_template = Suppress(
            Literal('Сумма=')
        ) + Word(nums + '.').setParseAction(lambda s, l, t: Decimal(t[0]))
        date_template = Suppress(
            Literal('Дата=')
        ) + Word(nums + '.').setParseAction(
            lambda s, l, t: timezone.datetime.strptime(t[0], '%d.%m.%Y'))
        unicodePrintables = u''.join(chr(c) for c in range(65536)
                                     if not chr(c).isspace())
        to_line_end = Word(unicodePrintables + ' ')
        payer_template = Suppress(Literal(
            'Плательщик1=')) + to_line_end
        receiver_template = Suppress(Literal(
            'Получатель1=')) + to_line_end
        comment_template = Suppress(Literal(
            'НазначениеПлатежа=')) + to_line_end
        for part in parts:
            summ = summ_template.searchString(part)[0][0]
            date = date_template.searchString(part)[0][0]
            payer = payer_template.searchString(part)[0][0]
            pay_receiver = receiver_template.searchString(part)[0][0]
            comment = comment_template.searchString(part)[0][0]
            if pay_receiver == 'ООО "Благовест"':
                self.records.create(
                    summ=summ,
                    date=date,
                    payer=payer,
                    comment=comment,
                )

    def save(self, *args, **kwargs):
        self.full_clean()
        super(BankFile, self).save(*args, **kwargs)
        self.parse_file()


# These two auto-delete files from filesystem when they are unneeded:
@receiver(models.signals.post_delete, sender=BankFile)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `BankFile` object is deleted.
    """
    if instance.f:
        if os.path.isfile(instance.f.path):
            os.remove(instance.f.path)


class BankRecord(models.Model):
    bank_file = models.ForeignKey(
        BankFile,
        verbose_name=u'Запись файла',
        related_name='records')
    summ = models.DecimalField(u'Сумма', max_digits=10, decimal_places=2)
    date = models.DateField(u'Дата', default=timezone.now)
    payer = models.CharField(u'Плательщик', max_length=1024)
    comment = models.TextField(u'Комментарий', null=True, blank=True)

    class Meta:
        verbose_name = "BankRecord"
        verbose_name_plural = "BankRecords"
        default_permissions = ()

    def __unicode__(self):
        return u"Запись #{} файла #{}".format(self.pk, self.bank_file_id)
