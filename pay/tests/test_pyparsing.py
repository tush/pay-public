from pyparsing import Word, Suppress, nums, alphas, SkipTo, \
    Literal, Forward, OneOrMore, FollowedBy, LineEnd, restOfLine, printables,\
    ParseException
from decimal import Decimal
from datetime import datetime


def test_summ():
    part = "Сумма=500.00"
    summ_template = (
        Suppress(Literal('Сумма=')) +
        Word(nums + '.').setParseAction(lambda s, l, t: Decimal(t[0])))
    assert summ_template.parseString(part).asList()[0] == Decimal('500.00')


def test_date():
    part = "Дата=27.10.2014"
    date_template = (
        Suppress(Literal('Дата=')) +
        Word(nums + '.').setParseAction(
            lambda s, l, t: datetime.strptime(t[0], '%d.%m.%Y')))
    assert date_template.parseString(
        part).asList()[0] == datetime(2014, 10, 27)


def test_reciever():
    unicodePrintables = u''.join(chr(c) for c in range(65536)
                                 if not chr(c).isspace())
    part = 'Получатель1= КБ "ЮНИАСТРУМ БАНК" (ООО)\n'
    to_line_end = Word(unicodePrintables + ' ')
    reciever_template = Suppress(Literal(
        'Получатель1=')) + to_line_end
    try:
        assert reciever_template.parseString(
            part).asList()[0] == 'КБ "ЮНИАСТРУМ БАНК" (ООО)'
    except ParseException as err:
        print(err.line)
        print(" " * (err.column - 1) + "^")
        print(err)
        raise err


def test_comment():
    unicodePrintables = u''.join(chr(c) for c in range(65536)
                                 if not chr(c).isspace())
    part = 'НазначениеПлатежа=Оплата 2-50% по счету N 1541 от 19.09.2014г. за книгу Молитвослов "Благослови, душе моя Господа", в т.ч. НДС(10%) - 27045-45.\n'
    to_line_end = Word(unicodePrintables + ' ')
    comment_template = Suppress(Literal(
        'НазначениеПлатежа=')) + to_line_end
    try:
        assert comment_template.parseString(
            part).asList()[0] == 'Оплата 2-50% по счету N 1541 от 19.09.2014г. за книгу Молитвослов "Благослови, душе моя Господа", в т.ч. НДС(10%) - 27045-45.'
    except ParseException as err:
        print(err.line)
        print(" " * (err.column - 1) + "^")
        print(err)
        raise err
