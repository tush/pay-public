
from metric.ftp_importer import FtpImporter, CrmImporter
import os
import datetime

from unittest.mock import patch


basepath = '/dashboard'
importer_class = CrmImporter


def test_parse_xml(importer):
    xml = open(os.path.join(os.path.dirname(__file__), 'xml/15-12-2014.xml'), 'rb').read()
    data = importer._parse_xml(xml)
    expected = {
        'order': {
            'count': 125,
            'summ': 625000.32,
        },
        'sell': {
            'count': 112,
            'summ': 442564.0,
        },
        'return': {
            'count': 126,
            'summ': 2100.00,
        },
    }
    assert expected == data


@patch.object(CrmImporter, '_parse_xml')
@patch.object(CrmImporter, '_get_xml')
def test_get_data_from_ftp(_get_xml, _parse_xml, ftp_client, base_path):
    importer = CrmImporter(ftp_client, base_path)
    date = datetime.datetime.strptime('2014-12-15', '%Y-%m-%d').date()
    _parse_xml.return_value = {'a': 1}
    _get_xml.return_value = b"<root/>"
    data = importer.get_data(date)
    assert {'a': 1} == data
    _parse_xml.assert_called_once_with(b"<root/>")
    _get_xml.assert_called_once_with(date)
