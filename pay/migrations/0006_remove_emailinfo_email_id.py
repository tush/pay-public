# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pay', '0005_auto_20150618_0940'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='emailinfo',
            name='email_id',
        ),
    ]
