import ftplib
import os
from io import BytesIO
from django.conf import settings


class FTPClient(object):

    def __init__(self):
        self._ftp = ftplib.FTP()
        self._ftp.connect(settings.FTP_HOST,
                          settings.FTP_PORT,
                          timeout=30
                          )
        result = self._ftp.login(user=settings.FTP_USER,
                                 passwd=settings.FTP_PASSWORD)
        assert result.startswith('230 ')
        self.encoding = 'utf-8'

    def dir(self, path=None):
        names = []
        for filename, description in self._ftp.mlsd(path):
            names.append(filename)
        return names

    def exists(self, path):
        accum = ['.']
        for chunk in path.split(os.path.sep):
            if chunk not in self.dir(os.path.join(*accum)):
                return False
            accum.append(chunk)
        return True

    def mkdir(self, dirname):
        accum = []
        for chunk in dirname.split(os.path.sep):
            accum.append(chunk)
            if not self.exists(os.path.join(*accum)):
                self._ftp.mkd(os.path.join(*accum))

    def store(self, path, content):
        dirname, _ = os.path.split(path)
        if dirname is not None and dirname != '.':
            self.mkdir(dirname)
        self._ftp.storbinary("STOR {}".format(path), BytesIO(content))

    def get(self, path):
        buf = BytesIO()
        try:
            self._ftp.retrbinary("RETR {}".format(path), buf.write)
        except ftplib.error_perm:
            raise IOError()
        buf.seek(0)
        return buf
