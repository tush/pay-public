from django.conf.urls import patterns, url
from django.contrib.auth.decorators import permission_required
from .views import ClaimCreate, ClaimUpdate, ClaimSpam, ClaimList, ManyClaimsAction

pr = permission_required('can_work_with_claims', raise_exception=True)

urlpatterns = patterns('',
                       url(r'^new/$', pr(ClaimCreate.as_view()), name='new-claim'),
                       url(r'^edit/(?P<pk>\d+)/$', pr(ClaimUpdate.as_view()), name='edit-claim'),
                       url(r'^spam/(?P<pk>\d+)/$', pr(ClaimSpam.as_view()), name='to-spam-claim'),
                       url(r'^multi/$', pr(ManyClaimsAction.as_view()), name='multi-action'),
                       url(r'^$', pr(ClaimList.as_view()), name='claim-list'),
                       )
