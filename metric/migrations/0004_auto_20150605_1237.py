# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('metric', '0003_auto_20150605_1205'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='metrikadata',
            options={'default_permissions': (), 'permissions': (('can_view_stat', 'Может просматривать статистику'),)},
        ),
    ]
