from django.test import TestCase


class ViewTest(TestCase):

    def test_open_pay_form(self):
        response = self.client.get('/pay')
        self.assertEqual(response.status_code, 403)
