from django.core.management.base import BaseCommand
from metric.models import MetrikaData
import datetime


class Command(BaseCommand):
    help = 'Обновление данных из метрики'

    def handle(self, *args, **options):
        today = datetime.date.today()
        for i in range(1, 7):
            date = today - datetime.timedelta(i)
            MetrikaData.update(date)
