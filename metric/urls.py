from django.conf.urls import patterns, url
from django.contrib.auth.decorators import permission_required
from functools import partial
from .views import MetricaCsvView, CrmCsvView, DinamicCsvView, TopCsvView

pr = partial(permission_required, raise_exception=True)

urlpatterns = patterns('',
                       # Examples:
                       url(r'^metrika/(?P<date1>\d{8})/(?P<date2>\d{8})/$', pr('metric.can_view_stat')
                           (MetricaCsvView.as_view()), name='metrika_csv_data'),
                       url(r'^crm/(?P<date1>\d{8})/(?P<date2>\d{8})/(?P<data_type>.+)/$',
                           pr('metric.can_view_stat')(CrmCsvView.as_view()), name='crm_csv_data'),
                       url(r'^dinamic/(?P<date1>\d{8})/(?P<date2>\d{8})/$', pr('metric.can_view_stat')
                           (DinamicCsvView.as_view()), name='dinamic_csv_data'),
                       url(r'^top_csv/(?P<date1>\d{8})/(?P<date2>\d{8})/$',
                           pr('metric.can_view_stat')(TopCsvView.as_view()), name='top_csv_data'),
                       )
