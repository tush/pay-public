from django.conf import settings
import requests
import logging

logger = logging.getLogger(__name__)


class GetTokenException(Exception):

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return "{error}: {error_description}".format(**self.message)


class NoTokenException(Exception):
    pass


def token_required(f):
    def wrapper(self, *args, **kwargs):
        if not self.token:
            raise NoTokenException()
        return f(self, *args, **kwargs)
    return wrapper


class Metrika(object):

    API_URL = 'http://api-metrika.yandex.ru/{method}.json'

    def __init__(self, token=None):
        self.token = token

    @property
    def confirm_code_url(self):
        return "https://oauth.yandex.ru/authorize?response_type=code&client_id={}".format(
            settings.METRIKA_APP_ID
        )

    def get_token(self, code):
        result = requests.post('http://oauth.yandex.ru/token', {
            'grant_type': 'authorization_code',
            'code': code,
            'client_id': settings.METRIKA_APP_ID,
            'client_secret': settings.METRIKA_APP_SECRET,
        })
        data = result.json()
        if 'error' in data:
            raise GetTokenException(data)
        self.token = data['access_token']
        return self.token

    def _make_query(self, path, args=None):
        url = self.API_URL.format(method=path.lstrip('/'))
        if self.token is not None:
            if args is None:
                args = {}
            args.update(oauth_token=self.token)
        r = requests.get(url, params=args)
        if r.status_code != 200:
            logger.error('Code: %s, message: "%s"', r.status_code, r.text)
        return r.json()

    @token_required
    def get_counters(self):
        return self._make_query("/counters")

    @token_required
    def get_day_data(self, counter_id, date):
        day_str = date.strftime('%Y%m%d')
        result = self._make_query("/stat/traffic/summary", {
            'id': counter_id,
            'date1': day_str,
            'date2': day_str,
        })
        return result['data'][0]
