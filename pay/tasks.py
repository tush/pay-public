from __future__ import absolute_import

from celery import shared_task


@shared_task(bind=True, max_retries=3, default_retry_delay=30)
def export_payinfo(self, content, filename):
    try:
        from .models import Exporter
        exporter = Exporter()
        exporter.export(filename, content)
    except Exception as exc:
            raise self.retry(exc=exc)
