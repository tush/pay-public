
import pytest


@pytest.fixture(scope="module")
def ftp_client():
    from pay.ftp import FTPClient
    from unittest import mock
    return mock.Mock(FTPClient)()


@pytest.fixture(scope="module")
def base_path(request):
    path = getattr(request.module, 'basepath', '/dashboard')
    return path


@pytest.fixture(scope="module")
def importer(request, ftp_client, base_path):
    importer_class = getattr(request.module, 'importer_class')
    return importer_class(ftp_client, base_path)
