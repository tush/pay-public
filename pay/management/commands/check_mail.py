from django.core.management.base import BaseCommand, CommandError
import traceback
from pay.models import EmailInfo
from pay.imap_fetcher import ImapFetcher
from django.utils import timezone

import email
from email.mime.base import MIMEBase

from django.conf import settings
from django.core.mail import EmailMessage


class Command(BaseCommand):
    help = 'Загрузка и обработка писем'

    def handle(self, *args, **options):
        fetcher = ImapFetcher(
            settings.IMAP_HOST,
            settings.IMAP_PORT,
            settings.IMAP_LOGIN,
            settings.IMAP_PASSWORD)
        date_from = timezone.datetime.strptime(
            settings.PAY_MAILS_DATE_FROM, '%Y.%m.%d')
        ids = set(fetcher.get_ids_from_date_for_senders(
            date_from, EmailInfo.SOURCES_EMAILS.keys()))
        processed_uids = set(EmailInfo.objects.values_list('email_uid', flat=True))
        for email_id in ids:
            uid = fetcher.get_uid_for_id(email_id)
            if uid in processed_uids:
                continue
            body = fetcher.get_email_content(email_id)
            if body is None:
                continue
            ei_id = None
            try:
                date = fetcher.get_message_date(email_id)
                ei = EmailInfo.create_from_string(uid, body, date)
                if ei.email_uid is None:
                    ei.email_uid = uid
                    ei.save()
                    continue
                ei_id = ei.id
                ei.save_pay()
                self.stdout.write(
                    'Saved message {} from {}'.format(uid, ei.sender))
            except Exception as e:
                self.stderr.write(repr(e))
                traceback.print_exc(file=self.stderr)
                error_mail_body = "{}\nId письма {}\nПисьмо во вложении.".format(e, ei_id)
                msg = EmailMessage(
                    'Ошибка обработки письма',
                    error_mail_body,
                    settings.DEFAULT_FROM_EMAIL,
                    settings.PARSE_ERROR_SEND_TO)
                rfcmessage = MIMEBase("message", "rfc822")
                message_to_forward = email.message_from_bytes(body)
                rfcmessage.attach(message_to_forward)
                msg.attach(rfcmessage)
                msg.send()
