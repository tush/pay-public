# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import metric.mixins


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Config',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('metrika_token', models.CharField(max_length=255, verbose_name='Токен Яндекс Метрики')),
                ('metrika_counter_id', models.CharField(max_length=32, verbose_name='Id счетчика Яндекс Метрики')),
            ],
            options={
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='CrmData',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('date', models.DateField(unique=True, verbose_name='Дата')),
                ('order_count', models.IntegerField(verbose_name='Количество заказов')),
                ('sell_count', models.IntegerField(verbose_name='Количество реализаций')),
                ('return_count', models.IntegerField(verbose_name='Количество возвратов')),
                ('order_summ', models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Сумма заказов')),
                ('sell_summ', models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Сумма реализаций')),
                ('return_summ', models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Сумма возвратов')),
            ],
            options={
                'abstract': False,
                'ordering': ['date'],
                'default_permissions': (),
            },
            bases=(metric.mixins.FtpUpdaterMixin, metric.mixins.UpdateAllMixin, models.Model),
        ),
        migrations.CreateModel(
            name='MetrikaData',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('date', models.DateField(unique=True, verbose_name='Дата')),
                ('visitors', models.IntegerField(verbose_name='Посетители')),
                ('page_views', models.IntegerField(verbose_name='Просмотры')),
                ('denial', models.FloatField(verbose_name='Отказы')),
                ('visit_time', models.IntegerField(verbose_name='Время на сайте')),
            ],
            options={
                'abstract': False,
                'ordering': ['date'],
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='Rest',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Дата')),
                ('rest', models.IntegerField(verbose_name='Остаток')),
                ('article', models.CharField(max_length=64, verbose_name='Артикул')),
                ('name', models.TextField(verbose_name='Название')),
                ('uom', models.CharField(max_length=32, verbose_name='Единица измерения')),
                ('imageaddress', models.URLField(max_length=1024, verbose_name='Адрес')),
                ('flagdiscountoff', models.BooleanField(verbose_name='*')),
                ('flagdiscountdis', models.BooleanField(verbose_name='!')),
            ],
            options={
                'default_permissions': (),
            },
            bases=(models.Model, metric.mixins.UpdateAllMixin, metric.mixins.FtpUpdaterMixin),
        ),
    ]
