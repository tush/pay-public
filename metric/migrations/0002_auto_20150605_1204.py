# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('metric', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='metrikadata',
            options={'permissions': ('can_view_stat', 'Может просматривать статистику')},
        ),
        migrations.AlterModelOptions(
            name='rest',
            options={'default_permissions': (), 'permissions': ('can_view_top', 'Может просматривать ТОП')},
        ),
    ]
