# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('claim', '0007_auto_20150613_1207'),
    ]

    operations = [
        migrations.AlterField(
            model_name='claim',
            name='sended_on',
            field=models.DateTimeField(verbose_name='Дата отправки', db_index=True, default=django.utils.timezone.now),
        ),
    ]
