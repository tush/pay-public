# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('claim', '0003_auto_20150605_1612'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='claim',
            options={'verbose_name': 'Обращение', 'verbose_name_plural': 'Обращения', 'default_permissions': (), 'ordering': ['-sended_on'], 'permissions': (('can_work_with_claims', 'Может работать с обращениями'),)},
        ),
        migrations.AlterField(
            model_name='claim',
            name='manager',
            field=models.ForeignKey(verbose_name='Менеджер', null=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
