
from metric.ftp_importer import FtpImporter
import os
import datetime
from io import BytesIO

from unittest.mock import patch


basepath = '/xml'
importer_class = FtpImporter


def test_create(importer):
    assert importer.client is not None
    assert basepath == importer.base_dir


def test_report_filename(importer):
    date = datetime.datetime.strptime('2014-12-15', '%Y-%m-%d').date()
    path = importer._get_report_path(date)
    assert basepath + '/15-12-2014.xml' == path


def test_fetch_date_data(importer, ftp_client):
    ftp_client.get.return_value = BytesIO(b'content')
    date = datetime.datetime.strptime('2014-12-15', '%Y-%m-%d').date()
    content = importer._get_xml(date)
    assert b'content' == content
    ftp_client.get.assert_called_once_with(basepath + '/15-12-2014.xml')


def test_crm_list_dates(importer):
    ftp = importer.client
    ftp.dir.return_value = [
        '15-12-2014.xml',
        '16-12-2014.xml',
        '17-12-2014.xml',
    ]
    dates = importer.scan_dates()
    assert 3 == len(dates), 'Отличается количество файлов'
    assert isinstance(dates[0], datetime.date)
    assert datetime.datetime(2014, 12, 15).date() in dates
    ftp.dir.assert_called_once_with(basepath)
