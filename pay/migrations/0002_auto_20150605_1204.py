# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pay', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='payinfo',
            options={'default_permissions': (), 'permissions': (('can_view_payinfo', 'Может просматривать журнал платежей'), ('can_add_payinfo', 'Может вносить оплаты')), 'verbose_name': 'PayInfo', 'verbose_name_plural': 'PayInfos'},
        ),
    ]
