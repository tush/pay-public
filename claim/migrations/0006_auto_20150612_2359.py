# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('claim', '0005_auto_20150606_0131'),
    ]

    operations = [
        migrations.AddField(
            model_name='claim',
            name='exported',
            field=models.BooleanField(default=False, verbose_name='Выгружено'),
        ),
        migrations.AddField(
            model_name='claim',
            name='seen',
            field=models.BooleanField(default=False, verbose_name='Просмотрено'),
        ),
    ]
