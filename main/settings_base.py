"""
Django settings for main project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from spirit.settings import *

ST_PRIVATE_FORUM = True
ST_RATELIMIT_ENABLE = False

import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '!zx=_yjirex7x9uhd)uahkl64$x%a=6gl9vj@q7t$*(khr+1'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = []

SITE_ID = 2


# Application definition

INSTALLED_APPS += (
    'django.contrib.sites',
)

INSTALLED_APPS += (
    'bootstrap3',
    'django_activeurl',
    'debug_toolbar',
)

INSTALLED_APPS += (
    'django.contrib.flatpages',
    'ckeditor'
)

DJANGO_WYSIWYG_FLAVOR = "ckeditor"
CKEDITOR_UPLOAD_PATH = "ckeditor/"
CKEDITOR_IMAGE_BACKEND = "pillow"
CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
FLUENT_TEXT_CLEAN_HTML = True
FLUENT_TEXT_SANITIZE_HTML = True

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'full',
        'width': '1200',
        'height': '600',
    },
}


INSTALLED_APPS += (
    'pay',
    'metric',
    'claim',
)

LOGIN_REDIRECT_URL = 'dashboard'
LOGIN_URL = 'login'

MIDDLEWARE_CLASSES += (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)

TEMPLATES[0]['DIRS'] = (os.path.join(BASE_DIR, 'main/templates'),)


ROOT_URLCONF = 'main.urls'

WSGI_APPLICATION = 'main.wsgi.application'

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'main/static'),

)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'upload')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        }
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
        },
        'null': {
            'class': 'django.utils.log.NullHandler',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'logs/pay.log'),
            'level': 'WARN',
            'formatter': 'verbose',
            'maxBytes': 10240,
            'backupCount': 3
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
        'django.security': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
        'pay': {
            'handlers': ['console', 'file', 'mail_admins'],
        },
        'metric': {
            'handlers': ['console', 'file', 'mail_admins'],
        },
        'py.warnings': {
            'handlers': ['console'],
        },
    }
}

from datetime import timedelta

CELERYBEAT_SCHEDULE = {
    'recheck_email': {
        'task': 'claim.tasks.check_mails',
        'schedule': timedelta(minutes=5),
    },
}

CELERY_TIMEZONE = 'UTC'
CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']

# IMAP CONFIG

IMAP_HOST = "mail2.itmatrix.ru"
IMAP_PORT = 143
IMAP_LOGIN = "login"
IMAP_PASSWORD = "password"
PAY_MAILS_DATE_FROM = "2014.11.01"

IMAP_CLAIM_LOGIN = "clain_login"
IMAP_CLAIM_PASSWORD = "claim_password"

EMAIL_HOST = IMAP_HOST
EMAIL_HOST_USER = IMAP_LOGIN
EMAIL_HOST_PASSWORD = IMAP_PASSWORD
DEFAULT_FROM_EMAIL = IMAP_LOGIN
PARSE_ERROR_SEND_TO = (
    'g.dyuldin@gmail.com',
)

# FTP
FTP_HOST = 'ftp_ip'
FTP_PORT = 21
FTP_USER = 'Ftpuser'
FTP_PASSWORD = 'ftpuser password'
FTP_FOLDER = 'private/PAY'
FTP_DASHBOARD_FOLDER = "private/DASHBOARD"
FTP_FOLDER_CLAIM = "private/CLAIM"

# Metrika
METRIKA_APP_ID = 'app_id'
METRIKA_APP_SECRET = 'app_secret'
METRIKA_CALLBACK_URL = 'https://oauth.yandex.ru/verification_code'

# local export
EXPORT_PATH = '/tmp/pay-test'
