from django.core.management.base import BaseCommand, CommandError
from metric.models import CrmData, Rest
from optparse import make_option


class Command(BaseCommand):
    help = 'Обновление данных по ftp'
    option_list = BaseCommand.option_list + (
        make_option('--top',
                    action='store_true',
                    dest='top',
                    default=False,
                    help='Получение остатков'),
        make_option('--crm',
                    action='store_true',
                    dest='crm',
                    default=False,
                    help='Получение сведений из 1С'),
    )

    def handle(self, *args, **options):
        if options['crm']:
            CrmData.update_all()
        elif options['top']:
            Rest.update_all()
        else:
            raise CommandError('Необходимо указать источник данных')
