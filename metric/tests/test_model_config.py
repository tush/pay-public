from django.test import TestCase
from metric.models import Config


class ConfigTest(TestCase):

    def test_get_config(self):
        Config.objects.create(metrika_token="23123")
        val = Config.get()
        assert "23123" == val.metrika_token

    def test_set_new_value(self):
        Config.objects.all().delete()
        Config.set(metrika_token='854')
        assert '854' == Config.get().metrika_token

    def test_update_exists_value(self):
        Config.set(metrika_token='255')
        assert '855' != Config.get().metrika_token
        Config.set(metrika_token='855')
        assert '855' == Config.get().metrika_token

    def test_get_first_time(self):
        Config.objects.all().delete()
        obj = Config.get()
        assert isinstance(obj, Config)
        assert obj.id is None
